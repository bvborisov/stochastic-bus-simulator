from math import log, exp, fabs

from collections import deque
from numpy import nextafter, inf, digitize, array
from numpy import add

from os import urandom
import random

try:
    import ipdb
except ImportError:
    import pdb

from simulation_instance import *
from simulator_io import update_progress, parse_input_file
# used by get_delay()
almost_zero = nextafter(0.0,inf) # float after 0.0 in the direction of + infinity


class SimulationExecution:
    '''Active book-keeping class. See README for more information'''
    def __init__(self,simulation_instance,seed=None,disembark_before_board=False,count_missed_passengers_multiple_times=False,print_events=True):
        self.simulation_instance = simulation_instance
        self.backup_simulation_instance = simulation_instance
        self.passengers = set([])
        # do we allow passengers to board only after all passengers on bus have disembarked?
        self.disembark_before_board = disembark_before_board 
        # do we count a passenger as missed twice if, say, he missed a bus, a new bus arrived and the passenger missed it again? 
        self.count_missed_passengers_multiple_times = count_missed_passengers_multiple_times
        self.missed_passengers_per_stop_dict = {}
        self.random_inst = None
        self.print_events = print_events
        # keeps bus (journey, total_passenger) pairs with string keys. Pairs are lists because they have to be mutable
        # and route (journey, total_passenger) pairs with int keys    ditto
        self.buses_journeys_dict = dict((x,[0,0]) for x in self.simulation_instance.buses.keys()) #put all buses key-values pairs
        self.buses_journeys_dict.update(dict((x,[0,0]) for x in self.simulation_instance.routes.keys())) #put all roads key-value pairs
        self.buses_journeys_dict['overall'] = [0,0] # put overall roads-key value pair
        if self.count_missed_passengers_multiple_times:
            # if we count missed passengers multiple times we init the dictionary with integer buckers
            self.missed_passengers_per_stop_dict = dict.fromkeys(self.simulation_instance.stops,long(0))
            # for overall passengers
            self.missed_passengers_overall = long(0)
        else:
            # if we count missed passengers only once we init the dictionary with empty sets
            self.missed_passengers_per_stop_dict = dict((x,set([])) for x in self.simulation_instance.stops)
            # for overall passengers
            self.missed_passengers_overall = set([])
        # Dictionary of dictionaries: 1st level keys are stops, and
        # every stop has a dictionary of buses with elements spent time on
        # the stop and the number of times they have visited that stop
        self.avg_stop_qt_dict = dict((x,None) for x in self.simulation_instance.stops.keys())
        for x in self.avg_stop_qt_dict.keys():
            self.avg_stop_qt_dict[x] = dict((z,[0, 0.0]) for z in self.simulation_instance.buses.keys()) # [num_visits on stop, time spent on stop]
        # [num_visits on stop, time spent on stop], buses are starting on stops
        # so they have visited, overall, all the stops initialy so we add that
        # to the num_visits field in the dictionary
        self.avg_stop_qt_dict['overall'] = [len(self.simulation_instance.buses), 0.0]
        self.avg_pas_wait_stop = dict((x,0.0) for x in self.simulation_instance.stops.keys())
        self.avg_pas_wait_routes = dict((x,0.0) for x in self.simulation_instance.routes.keys())

        if seed is None:
            # setting the random seed to some pretty long 1024 byte string - more than enough to produce things randomnly enough
            # assuming the people who wrote random did their job well enough...
            try:
                self.random_inst = random.Random()
                self.random_inst.seed(urandom(1024))
            except NotImplementedError:
                self.random_inst = random.Random()
                self.random_inst.seed()
        else:
            self.random_inst = random.Random()
            self.random_inst.seed(seed)
        self.time = 0.0
        self.simulation_string = ''
        self.missed_passengers_overall = None
        # for create_passenger optimisation, avoids repeated recomputations
        self.stop_keys_array = self.simulation_instance.stops.keys() 
        # for create_passenger optimisation, avoids repeated recomputations
        self.stops_routes = {} 
        for x,y in self.simulation_instance.stops.items():
            self.stops_routes[x] = list(y.routes)
        # for create_passenger optimisation, avoids repeated recomputations
        self.routes_stops = {} 
        for x,y in self.simulation_instance.routes.items():
            self.routes_stops[x] = list(y.stops)
        # for run_simulation optimisation, avoids repeated recomputations
        self.pick_event = {"PassengerEntersSimulation":self.pas_enters,
                        "PassengerDisembarksBus":self.pas_disems,
                        "PassengerBoardsBus":self.pas_boards,
                        "BusDepartsStop":self.bus_depts,
                        "BusArrivesAtStop":self.bus_arrvs}

    def get_delay(self,total_rate):
        mean = (1.0/total_rate)
        # true zero 0.0 will give an exception if used with
        # log and thus we give it the float right after 0
        # the specs say random(0,1) inclusively but if we ever sample
        # a 0, we'd wait an infinite amount of time (theoretically)
        # and we'd get an exception so this is close enough for now
        p = self.random_inst.uniform(almost_zero,1.0)
        return -mean*log(p)
            
    # idea taken from : http://stackoverflow.com/a/11373929
    # but modified and extended since code was faulty 
    def discrete_dist_sample(self,possible_events,rates):
        # sort events by their rates if we do not do this then even
        # though we might have the same probabilities, if the events
        # generated are in a different order, this will return
        # different events!!!  therefore we sort events by their
        # rates(i.e. probabilities) and we have them in a totally
        # deterministic order and hence the method becomes totally
        # deterministic given a PRNG.
        probabilites = array(normalise(rates))
        possible_events = array(possible_events)
        # these are the intervals into which our probabilities fall
        # when a sample would be drawn from 0 to 1.0, if it falls in a
        # bin interval from a to b then that corresponding event is
        # selected
        bins = add.accumulate(probabilites) 
        samples = [self.random_inst.uniform(0.0,1.0)] #out one sample
        # this would give the index into possible_events
        # which correspond to the appropriate bin being selected i.e. the appropriate event
        return possible_events[digitize(array(samples), bins)][0] 
        
    def create_passenger(self):
        '''This method creates a passenger with a random destination and origin stops that are guaranteed to be different and on the same route.'''
        cur_pas = Passenger()
        origin_stop = self.random_inst.choice(self.stop_keys_array) 
        destination_route = self.random_inst.choice(self.stops_routes[origin_stop])
        destination_route_stop_list = list(self.routes_stops[destination_route])
        destination_route_stop_list.remove(origin_stop)
        destination_stop = self.random_inst.choice(destination_route_stop_list)
        cur_pas.set_origin_stop(origin_stop)
        cur_pas.set_destination_route(destination_route)
        cur_pas.set_destination_stop(destination_stop)
        cur_pas.set_state(('stop',origin_stop))
        return cur_pas

    def position_buses(self):
        '''This method positions the buses on the stop queues according to the rule that bus X.Y begins at stop Y mod [X].'''
        ident_bus_list = [x.identifier for x in self.simulation_instance.buses.values()]
        ident_bus_list.sort()
        cur_route = ident_bus_list[0][0]
        position = 0
        for ident in ident_bus_list: # ident is of form X.Y, and is of type string (X = route, Y = bus num)
            if ident[0] != cur_route: # we are now looking at the buses on another route
                cur_route = ident[0]
                position = 0
            num_stops = len(self.simulation_instance.routes[int(cur_route)].stops)
            self.simulation_instance.stops[self.simulation_instance.routes[int(cur_route)].stops_order[position]].bus_queue.append(ident) # put a bus at the head of a queue on stop number position
            state = ('stop',self.simulation_instance.stops[self.simulation_instance.routes[int(cur_route)].stops_order[position]].number) # set the bus state to the stop it was put on
            self.simulation_instance.buses[ident].set_state(state)
            position = (position + 1) % num_stops # stop positioning is modulo num_stops
            # when buses start off, they have visited exectly once the stop they start from
            # so we update this here
            self.avg_stop_qt_dict[state[1]][ident][0] += 1
                
    def run_simulation(self):
        '''Simulation loop, following the handout specification'''
        simulation_string = ''
        time = 0.0
        
        while time < self.simulation_instance.stop_time:
            update_progress(self.print_events,time/self.simulation_instance.stop_time)
            possible_events_and_rates = self.get_possible_events_and_their_rates(time) 
            possible_events_and_rates.sort(key=lambda x: x[1]) 
            [possible_events,rates] = unzip(possible_events_and_rates)
            total_rate = sum(rates) 
            delay = self.get_delay(total_rate) 
            # we don't allow the simulation to proceed if the delay will cause it to execute for
            # more than the stop time. This was not clearly specified
            # but since we sample from the exponential distribution the delay
            # can be (theoretically although it cant really be since we use floats), INFINITE,
            # and thus we should not allow it to execute. 
            # If this is not present, the average number of passengers that enter
            # a simulation is 1 greter than the rate * time 
            if time + delay >  self.simulation_instance.stop_time:
                update_progress(self.print_events,1)
                break
            # WE NEED THIS SO THAT EVENTS START AT THEIR APPROPRIATE TIMES
            time += delay
            # average bus queueing time statistics here
            iter_keys = self.avg_stop_qt_dict.keys()
            iter_keys.remove('overall')
            for x in iter_keys:
                for y in self.avg_stop_qt_dict[x].keys():
                    # if the current bus y is NOT on the current stop x's bus_queue HEAD
                    # we add some delay AND the queue is not empty
                    if len(self.simulation_instance.stops[x].bus_queue) > 1 and y != self.simulation_instance.stops[x].bus_queue[0] and y in list(self.simulation_instance.stops[x].bus_queue): #relying on a short-circuiting and operator since if the first is false, the second will throw should they both evauate..
                        self.avg_stop_qt_dict[x][y][1] += delay
                        self.avg_stop_qt_dict['overall'][1] += delay #adding more global delay
            # average waiting passengers here
            for x in self.avg_pas_wait_stop.keys():
                self.avg_pas_wait_stop[x] += len(self.simulation_instance.stops[x].passenger_set)*delay
            for x in self.avg_pas_wait_routes.keys():
                for y in self.simulation_instance.routes[x].stops:
                    pas_waiting_for_route = filter(lambda z: z.destination_stop in self.simulation_instance.routes[x].stops, self.simulation_instance.stops[y].passenger_set)
                    self.avg_pas_wait_routes[x] += len(pas_waiting_for_route)*delay
            # samples events probabilistically from the distribution formed by normalising the event rates
            chosen_event = self.discrete_dist_sample(possible_events,rates)
            chosen_event.set_time(time)
            simulation_string += chosen_event.get_description()
            # modify state based on event chosen
            self.pick_event[chosen_event.__class__.__name__](chosen_event)
        self.time = time
        self.simulation_string = simulation_string
        return simulation_string 
    
    def pas_enters(self,chosen_event):
        # add passenger to the set of passengers of the origin stop, and to the global passengers set
        self.simulation_instance.stops[chosen_event.passenger.origin_stop].passenger_set.add(chosen_event.passenger)
        self.passengers.add(chosen_event.passenger)
        return
    def pas_disems(self,chosen_event):
        # we need to remove the passenger from the bus' set
        self.simulation_instance.buses[chosen_event.bus.identifier]._passengers.remove(chosen_event.passenger)
        # we need to remove the passenger from the global passenger set
        self.passengers.remove(chosen_event.passenger)
        return
    def pas_boards(self,chosen_event):
        # this removing/adding pattern is because our passengers are 
        # in a set and if we mutate the elements of a set the hash 
        # change will break the set. That's why we remove an element, 
        # modify it, and then stick it back. Set operation is avg/worst O(log(n))
        # while a dict would to O(1) average so this might be a performance issue
        self.passengers.remove(chosen_event.passenger)
        # remove passenger from origin stop set
        self.simulation_instance.stops[chosen_event.passenger.origin_stop].passenger_set.remove(chosen_event.passenger)
        chosen_event.passenger.set_state(('bus',chosen_event.bus.identifier))
        self.passengers.add(chosen_event.passenger)
        self.simulation_instance.buses[chosen_event.bus.identifier].add_passenger(chosen_event.passenger)
        return
    def bus_depts(self,chosen_event):
        # we need to remove the bus from the stop queue - it might not be the bus at the HEAD of the queue though!
        bus_queue = list(self.simulation_instance.stops[chosen_event.stop].bus_queue)
        bus_queue.remove(chosen_event.bus.identifier)
        bus_queue = deque(bus_queue)
        self.simulation_instance.stops[chosen_event.stop].bus_queue = bus_queue
        # we need to change the bus' state to an appropriate road now i.e. the next road to be visited on the route
        cur_stop_index = chosen_event.bus._route.stops_order.index(chosen_event.bus.state[1]) #index of cur stop in stop_order
        next_stop = chosen_event.bus._route.stops_order[(cur_stop_index+1) % len(chosen_event.bus._route.stops_order)] # next stop
        # missed passengers added here
        # first case: assume that passengers can be missed multiple times: i.e. if a passenger missed a bus
        # and then another bus arrives and it misses that bus, then we count this as 2 misses rather than 1
        pass_list = self.simulation_instance.stops[chosen_event.bus.state[1]].get_passengers_waiting_for_bus(chosen_event.bus)
        
        if (self.count_missed_passengers_multiple_times):
            self.missed_passengers_per_stop_dict[chosen_event.bus.state[1]] += len(pass_list)
        else:
            # we do not count passengers twice so simply take union of sets of passengers already missed and passengers
            # who just missed the bus
            self.missed_passengers_per_stop_dict[chosen_event.bus.state[1]] = self.missed_passengers_per_stop_dict[chosen_event.bus.state[1]] | set(pass_list)

        # update bus's state. NOTE: this will update both the chosen_event's state and the bus in the
        # simulation execution since they hold the same reference
        self.simulation_instance.buses[chosen_event.bus.identifier].set_state(('road',(chosen_event.bus.state[1],next_stop)))
        # we calculate average passengers per bus per road
        # here since we are sure that a bus departs event has
        # transpired
        self.buses_journeys_dict[chosen_event.bus.identifier][0] += 1 # increase journeys for particular bus by 1
        self.buses_journeys_dict[chosen_event.bus.identifier][1] += len(chosen_event.bus._passengers) # increase passengers for particular bus by current num of passengers aboard
        self.buses_journeys_dict[chosen_event.bus._route.number][0] += 1 # increase journeys for bus' route by 1
        self.buses_journeys_dict[chosen_event.bus._route.number][1] += len(chosen_event.bus._passengers) # increase passengers for bus' route by current num of passengers aboard
        self.buses_journeys_dict['overall'][0] += 1 # increase overall journeys by 1
        self.buses_journeys_dict['overall'][1] += len(chosen_event.bus._passengers) # increase overall number of passengers
        return
    def bus_arrvs(self,chosen_event):
        # we need to change the bus' state to the appropriate stop now
        self.simulation_instance.buses[chosen_event.bus.identifier].set_state(('stop',chosen_event.bus.state[1][1]))
        # we need to add the bus to the stop's queue
        self.simulation_instance.stops[chosen_event.stop].bus_queue.append(chosen_event.bus.identifier)
        # we need to update the number of times this stop was visited by this bus by 1
                
        self.avg_stop_qt_dict[self.simulation_instance.buses[chosen_event.bus.identifier].state[1]][chosen_event.bus.identifier][0] += 1
        self.avg_stop_qt_dict['overall'][0] += 1
        return

    def get_statistics(self):
        if self.time == 0.0:
            try:
                inf = float('inf')
            except:  # check for a particular exception here?
                inf = 1e30000
            self.missed_passengers_overall = inf
            return "EMPTY SIMULATION"
        # Add missed passengers statistics to simulation_string
        # we first calculate missed passengers per stop
        missed_pas_stop_str = ''
        all_missed_pass_stops = 0
        for x,y in self.missed_passengers_per_stop_dict.items():
            missed_pas_stop_str += "number of missed passengers stop " + str(x) + " " + str(self.unpack_los(y)) + "\n"
            all_missed_pass_stops += self.unpack_los(y)
        # we then calculate missed passengers per route using the missed passengers per stop calculation
        missed_pass_routes_str = ''
        for x in self.simulation_instance.routes.values():
            cur_route_miss_pass = 0
            for y in x.stops:
                cur_route_miss_pass += self.unpack_los(self.missed_passengers_per_stop_dict[y])
            missed_pass_routes_str += "number of missed passengers route " + str(x.number) + " " + str(cur_route_miss_pass) + "\n"
        # we finally calculate missed passengers overall
        missed_pas_overall_str = "number of missed passengers " + str(all_missed_pass_stops) + "\n"
        # setting this for later retrieval
        self.missed_passengers_overall = all_missed_pass_stops
        
        # we add average passengers per bus per road here
        avg_pas_per_bus_str = ''
        for x in self.simulation_instance.buses.keys():
            if self.buses_journeys_dict[x][1] == self.buses_journeys_dict[x][0] == 0:
                avg_pas_per_bus_str += "average passengers bus " + x + " " + str(0.0) + "\n"
                continue
            else:
                avg_pas_per_bus_str += "average passengers bus " + x + " " + str(self.buses_journeys_dict[x][1]*1.0/self.buses_journeys_dict[x][0]) + "\n"
        avg_pas_per_route_str = ''

        for x in self.simulation_instance.routes.keys():
            if self.buses_journeys_dict[x][1] == self.buses_journeys_dict[x][0] == 0:
                avg_pas_per_route_str += "average passengers route " + str(x) + " " + str(0.0) + "\n"
            else:
                avg_pas_per_route_str += "average passengers route " + str(x) + " " + str(self.buses_journeys_dict[x][1]*1.0/self.buses_journeys_dict[x][0]) + "\n"
        if self.buses_journeys_dict['overall'][1] == 0 and self.buses_journeys_dict['overall'][0] == 0:
            avg_pas_overall = "average passengers " + str(0) + "\n"
        else:
            avg_pas_overall = "average passengers " + str(self.buses_journeys_dict['overall'][1]*1.0/self.buses_journeys_dict['overall'][0]) + "\n"
        # avg bus queuing times
        avg_stop_qt_str = ''
        iter_items = self.avg_stop_qt_dict.items()

        iter_items = filter(lambda x: x[0] != 'overall', iter_items)
        # x is stop num, y is bus dict
        for x,y in iter_items:
            cur_stop_avg_qt = 0.0
            for w in y.values():
                # some buses will NEVER go to some stops since they are on different routes
                # if that was the case, we just skip iteration
                if w[0] == 0 and w[1] == 0.0:
                    continue
                else:
                    cur_stop_avg_qt += w[1] / w[0]
            avg_stop_qt_str += "average queueing at stop " + str(x) + " " + str(cur_stop_avg_qt) + "\n"
        avg_stop_qt_str += "average queueing at all stops " + str(self.avg_stop_qt_dict['overall'][1] / self.avg_stop_qt_dict['overall'][0]) + "\n"
        # average passengers waiting here
        avg_pas_wait_stop_str = ''
        avg_pas_wait_overall = 0.0

        for x,y in self.avg_pas_wait_stop.items():
            avg_pas_wait_stop_str += "average waiting passengers at stop " + str(x) + " " + str(y / self.time) + "\n"
            avg_pas_wait_overall += y / self.time
        for x,y in self.avg_pas_wait_routes.items():
            avg_pas_wait_stop_str += "average waiting passengers on route " + str(x) + " " + str(y / self.time) + "\n"
        avg_pas_wait_stop_str += "average waiting passengers " + str(avg_pas_wait_overall) + "\n"
        return missed_pas_stop_str + missed_pass_routes_str + missed_pas_overall_str + avg_pas_per_bus_str + avg_pas_per_route_str + avg_pas_overall +  avg_stop_qt_str + avg_pas_wait_stop_str 
    
    def get_cost(self):
        return self.missed_passengers_overall * (self.simulation_instance.board + self.simulation_instance.disembarks)
                                                       
    def get_possible_events_and_their_rates(self,time):
        possible_events_and_rates = []
        # Passenger enters event - always enabled so we first add it
        cur_pas = self.create_passenger()
        cur_pas_enters_event = PassengerEntersSimulation(cur_pas)
        possible_events_and_rates.append((cur_pas_enters_event , self.simulation_instance.new_passengers)) 
        # Passenger boards event 
        for pas in self.passengers:
        # passengers on buses can't board
            if pas.state[0] == "bus": 
                continue
            # passengers cant board if there's no bus
            if len(self.simulation_instance.stops[pas.origin_stop].bus_queue) == 0: 
                continue
            cur_top_bus = self.simulation_instance.stops[pas.origin_stop].bus_queue.popleft()
            # Experimental feature: passengers may only board if no passengers on the bus
            # at the head of the queue want to disembark
            if self.disembark_before_board and self.simulation_instance.buses[cur_top_bus].are_passengers_waiting_to_disembark_cur_stop():
                # since we would skip the iteration, we would never push back the bus we popped
                self.simulation_instance.stops[pas.origin_stop].bus_queue.appendleft(self.simulation_instance.buses[cur_top_bus].identifier)
                continue
            # if the route the passenger wants to be on is the same
            # as the top bus' and the number of passengers in the bus
            # has to be < than the capacity and not <= since
            # we add 1 more passenger i.e. if the condition is <= , we
            # would add 1 more passenger and we would be over the capacity
            if pas.destination_route == self.simulation_instance.buses[cur_top_bus]._route.number and self.simulation_instance.buses[cur_top_bus].has_capacity():
                cur_pas_boards_event = PassengerBoardsBus(pas,self.simulation_instance.buses[cur_top_bus])
                possible_events_and_rates.append((cur_pas_boards_event,self.simulation_instance.board))
            self.simulation_instance.stops[pas.origin_stop].bus_queue.appendleft(self.simulation_instance.buses[cur_top_bus].identifier)
        # buses departing, ariving, and passengers disembarking are
        # mutually exclusive events and thus we use an elif to speed things up
        for bus in self.simulation_instance.buses.values():
            # Buses departing event
            if bus.state[0] == 'stop' and not bus.are_passengers_waiting_to_disembark_cur_stop() and (not self.simulation_instance.stops[bus.state[1]].are_passengers_waiting_for_bus(bus) or not bus.has_capacity()):
                # Since there's nothing to stop the bus from leaving we create the leave event now
                cur_bus_depart_event = BusDepartsStop(bus)
                possible_events_and_rates.append((cur_bus_depart_event,self.simulation_instance.departs))
            # Bus arrives event
            elif bus.state[0] == 'road':
                cur_bus_arrives_event = BusArrivesAtStop(bus)
                roads = [road for road in self.simulation_instance.roads if road.first_stop.number==bus.state[1][0] and road.second_stop.number==bus.state[1][1]]
                possible_events_and_rates.append((cur_bus_arrives_event,roads[0].rate))
            # Passengers disembark event
            elif bus.state[0] == 'stop' and bus.are_passengers_waiting_to_disembark_cur_stop():
                # the passengers who want to disembark on the current stop
                pass_list = bus.passengers_waiting_to_disembark_cur_stop()
                cur_pas_disembarks_events = []
                for pas in pass_list:
                    cur_pas_disembarks_events.append(PassengerDisembarksBus(pas,bus))
                for event in cur_pas_disembarks_events:
                    possible_events_and_rates.append((event,self.simulation_instance.disembarks))
        return possible_events_and_rates
    
        
    # This method will assert some post-simulation properties to verify that 
    # the simulation is producing plausible and possible events
    def post_simulation_sanity(self):
        num_pass_on_buses_and_stops = 0
        passengers_on_buses = set([])
        passengers_on_diff_buses = [] # list of sets, each being the passenger set of a different bus
        passengers_on_diff_stops = [] # list of sets, each being the passenger set of a different stop
        for bus in self.simulation_instance.buses.values():
            num_pass_on_buses_and_stops += len(bus._passengers) 
            passengers_on_diff_buses.append(bus._passengers)
            passengers_on_buses |= bus._passengers
            assert len(bus._passengers) <= bus.capacity, "Buses can't carry more passengers than their capacity"
            for pas in bus._passengers:
                assert pas.state[1] == bus.identifier , "All passengers on a bus must be on that bus"
            if bus.state[0] == 'road':
                for stop in self.simulation_instance.stops.values():
                    bus_queue = stop.bus_queue
                    assert bus.identifier not in list(bus_queue), "A bus on a road is not on any stop"
        passengers_on_stops = set([])
        for stop in self.simulation_instance.stops.values():
            num_pass_on_buses_and_stops += len(stop.passenger_set)
            passengers_on_stops |= stop.passenger_set
            passengers_on_diff_stops.append(stop.passenger_set)
        assert len(self.passengers) == num_pass_on_buses_and_stops, "Number of passengers in the simulation should be the number of passengers on all buses and on all stops"
        assert passengers_on_buses.isdisjoint(passengers_on_stops), "Passengers on stops must be disjoint from passengers on buses"
        assert passengers_on_buses | passengers_on_stops == self.passengers, "Union of passengers on buses and stops must be global passenger set"
        # this is confirm that no two passenger sets on any bus or stop contain the same passengers
        # and that also a passenger set on a bus or stop is a subset of the global passenger set
        for x in passengers_on_diff_stops:
            for y in passengers_on_diff_buses:
                assert x.isdisjoint(y), "Every pair of bus - stop passenger sets must be disjoint"
                assert y.issubset(self.passengers) , "Every bus passenger set must be a subset of the global passenger set"
            assert x.issubset(self.passengers) , "Every stop passenger set must be a subset of the global passenger set"
        # all stop passenger sets must be disjoint
        for x in passengers_on_diff_stops:
            for y in passengers_on_diff_stops:
                if x != y:
                    assert x.isdisjoint(y), "All stop passenger sets must be disjoint"
        # all bus passenger sets must be disjoint
        for x in passengers_on_diff_buses:
            for y in passengers_on_diff_buses:
                if x != y:
                    assert x.isdisjoint(y), "All bus passenger sets must be disjoint"
        return 
    # this method clears the simulation - it restores the simulation_instance to the one that is originally read
    # in by the constructor and it calls position_buses() to set up the buses
    def clear_simulation(self):
        self.simulation_instance = self.backup_simulation_instance
        self.passengers = set([])
        self.position_buses()
    
    # this nifty method unpacks the argument it was given according to how
    # the flag count_missed_passengers_multiple_times is set
    def unpack_los(self,long_or_set):
        if self.count_missed_passengers_multiple_times:
            return long(long_or_set) # defensive : this will throw if this is set
        else:
            return len(long_or_set)  # defensive : this will throw if this is long

# make a list of of tuples into two lists
def unzip(list_of_tuples):
    return [list(t) for t in zip(*list_of_tuples)]

# normalises a vector i.e. makes it a valid propability distribution
def normalise(list_of_rates):
    n = float(sum(list_of_rates))
    return map(lambda x: x/n, list_of_rates)

# Event related classes

class PassengerEntersSimulation():
    def __init__(self,passenger):
        self.passenger = passenger
        self.time = None
    def set_time(self,time):
        self.time = time
    def get_description(self): 
        return "A new passenger enters at stop "+str(self.passenger.origin_stop)+" with destination "+str(self.passenger.destination_stop)+" at time "+str(self.time) + "\n"

class PassengerBoardsBus():
    def __init__(self,passenger,bus):
        self.passenger = passenger
        self.origin_stop = passenger.origin_stop
        self.destination_stop = passenger.destination_stop
        self.bus = bus
        self.time = None # time is not available at event creation
    def set_time(self,time):
        self.time = time
    def get_description(self): 
        return "Passenger boards bus " + str(self.bus.identifier) + " at stop " + str(self.passenger.origin_stop) +" with destination " + str(self.passenger.destination_stop) + " at time " + str(self.time) + "\n"
        

class BusDepartsStop():
    def __init__(self,bus):
        self.bus = bus
        self.stop = bus.state[1]
        self.time = None
    def set_time(self,time):
        self.time = time
    def get_description(self): 
        return "Bus " + str(self.bus.identifier) + " leaves stop " + str(self.stop) + " at time " + str(self.time) + "\n"

class BusArrivesAtStop():
    def __init__(self,bus):
        self.bus = bus
        self.stop = bus.state[1][1]
        self.time = None
    def set_time(self,time):
        self.time = time
    def get_description(self): 
        return "Bus " + str(self.bus.identifier) + " arrives at stop " + str(self.stop) + " at time " + str(self.time) + "\n"


class PassengerDisembarksBus():
    def __init__(self,passenger,bus):
        self.passenger = passenger
        self.bus = bus
        self.time = None
    def set_time(self,time):
        self.time = time
    def get_description(self): 
        return "Passenger disembarks bus " + str(self.bus.identifier) + " at stop " + str(self.passenger.destination_stop) + " at time " + str(self.time) + "\n"
