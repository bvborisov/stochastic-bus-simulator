#!/usr/bin/python
from collections import deque
import unittest

try:
        import ipdb
except ImportError:
        import pdb

from simulation_execution import *
from simulation_instance import *
from simulator_io import *

class TestSimulatorIO(unittest.TestCase):
        def setUp(self):
                self.maxDiff = None
		self.output1 = {'board': 1.0,
		'depart': 3.0,
		'disembark': 3.0,
		'ignore warnings': False,
		'new_passenger': 8.0,
		'optimise parameters': False,
		'roads': [('1', '2', '0.3'), ('2', '3', '0.4'), ('3', '1', '0.5')],
		'routes': [('1', ['1', '2', '3'], '4', '50')],
                'stop_time': 100.0,
                'description':''}

		self.output2 = {'board': 1.0,
		'depart': 3.0,
		'disembark': 3.0,
		'ignore warnings': False,
		'new_passenger': 8.0,
		'optimise parameters': False,
		'roads': [('1', '2', '0.3'),
		('2', '3', '0.4'),
		('3', '1', '0.5'),
		('2', '4', '0.1'),
		('4', '5', '0.7'),
		('5', '2', '0.8')],
		'routes': [('2', ['1', '2', '3'], '4', '50'),
		('3', ['2', '4', '5'], '7', '45')],
                'stop_time': 100.0,
                'description':''}

		self.output3 = {'board': 1.0,
		'depart': 3.0,
		'disembark': 3.0,
		'ignore warnings': True,
		'new_passenger': 8.0,
		'optimise parameters': False,
		'roads': [('1', '2', '0.3'),
		('2', '3', '0.4'),
		('3', '1', '0.5'),
		('2', '4', '0.1'),
		('4', '5', '0.7'),
		('5', '2', '0.8')],
		'routes': [('2', ['1', '2', '3'], '4', '50'),
		('3', ['2', '4', '5'], '7', '45')],
                'stop_time': 100.0,
                'description':''}

		self.output4 ={'board': 1.0,
		'depart': 3.0,
		'disembark': 2.0,
		'ignore warnings': True,
		'new_passenger': 4.0,
		'optimise parameters': False,
		'roads': [('1', '2', '0.3'), ('2', '3', '0.4'), ('3', '1', '0.5')],
		'routes': [('1', ['1', '2', '3'], '4', '50')],
                'stop_time': 500.0,
                'description':''}
                
                self.output7 = {'board': 1.0,
                'depart': 3.0,
                'disembark': 2.0,
                'ignore warnings': False,
                'new_passenger': 4.0,
                'optimise parameters': False,
                'roads': [('1', '2', '1.0'), ('2', '1', '1.0')],
                'routes': [('1', ['1', '2'], '2', '5')],
                'stop_time': 5.0,
                'description':''}


	def test_all_input_present(self):
		'''A test that verifies that we can successfully parse a regular file i.e. when all lines of input are present in the file - we have roads, buses, routes, etc.. and all necessary control lines exactly once. '''
		self.assertEqual(self.output1, parse_input_file('./test_files/simple1.text')[0])
		# the same test, simply uses different test files - althoug repetitive, 2 more tests won't hurt !
		self.assertEqual(self.output2, parse_input_file('./test_files/simple2.text')[0])
		self.assertEqual(self.output3, parse_input_file('./test_files/simple3.text')[0])
		self.assertEqual(self.output4, parse_input_file('./test_files/simple4.text')[0])
                self.assertEqual(self.output7, parse_input_file('./test_files/simple7.text')[0])

	
	def test_raises(self):
		''' This test captures the behavior that when a control line (i.e. boards, disemabarks ... )is not present we should raise an exception '''
		self.assertRaises(SimulationInputError, parse_input_file, 'test_files/no_board.text')
		self.assertRaises(SimulationInputError, parse_input_file, 'test_files/no_disembarks.text')
		self.assertRaises(SimulationInputError, parse_input_file, 'test_files/no_departs.text')
		self.assertRaises(SimulationInputError, parse_input_file, 'test_files/no_new_passengers.text')
		self.assertRaises(SimulationInputError, parse_input_file, 'test_files/no_stop_time.text')
		self.assertRaises(SimulationInputError, parse_input_file, 'test_files/empty.text')

		self.assertRaises(IOError, parse_input_file, './test_files/non_existent_file')

	def test_complex_comments(self):
		'''This test tests whether our parser can handle complex inlined comments like the one below(see source) it also checks whether we can handle comment lines in between road/route/control lines and it also checks if our parser accidentally captures control lines hidden in comments'''
		self.assertEqual(self.output1, parse_input_file('./test_files/complex_comments1.text')[0]) # like this one
		self.assertEqual(self.output1, parse_input_file('./test_files/complex_comments2.text')[0])
        def test_experiments(self):
                '''This tests the experimentation feature. If an experiments line is present, then parse_input_file() should return a list of dictionaries that represent the different combinations of simulation_instances that occur from all possible combinations of experimentations.'''
                simple11 = [{'board': 1.0,
  'depart': 3.0,
  'description': 'route 1 stops 1 2 buses 2 capacity 5\nroad 1 2 1.0\ndisembarks 2.0\n',
  'disembark': 2.0,
  'ignore warnings': False,
  'new_passenger': 4.0,
  'optimise parameters':True,
  'roads': [('1', '2', '1.0'), ('2', '1', '1.0')],
  'routes': [('1', ['1', '2'], '2', '5')],
  'stop_time': 5.0},
 {'board': 1.0,
  'depart': 3.0,
  'description': 'route 1 stops 1 2 buses 2 capacity 5\nroad 1 2 1.0\ndisembarks 8.1\n',
  'disembark': 8.0999999999999996,
  'ignore warnings': False,
  'new_passenger': 4.0,
  'optimise parameters': True,
  'roads': [('1', '2', '1.0'), ('2', '1', '1.0')],
  'routes': [('1', ['1', '2'], '2', '5')],
  'stop_time': 5.0},
 {'board': 1.0,
  'depart': 3.0,
  'description': 'route 1 stops 1 2 buses 2 capacity 5\nroad 1 2 1.0\ndisembarks 4.3\n',
  'disembark': 4.2999999999999998,
  'ignore warnings': False,
  'new_passenger': 4.0,
  'optimise parameters': True,
  'roads': [('1', '2', '1.0'), ('2', '1', '1.0')],
  'routes': [('1', ['1', '2'], '2', '5')],
  'stop_time': 5.0},
 {'board': 1.0,
  'depart': 3.0,
  'description': 'route 1 stops 1 2 buses 2 capacity 5\nroad 1 2 4.1\ndisembarks 2.0\n',
  'disembark': 2.0,
  'ignore warnings': False,
  'new_passenger': 4.0,
  'optimise parameters': True,
  'roads': [('1', '2', '4.1'), ('2', '1', '1.0')],
  'routes': [('1', ['1', '2'], '2', '5')],
  'stop_time': 5.0},
 {'board': 1.0,
  'depart': 3.0,
  'description': 'route 1 stops 1 2 buses 2 capacity 5\nroad 1 2 4.1\ndisembarks 8.1\n',
  'disembark': 8.0999999999999996,
  'ignore warnings': False,
  'new_passenger': 4.0,
  'optimise parameters': True,
  'roads': [('1', '2', '4.1'), ('2', '1', '1.0')],
  'routes': [('1', ['1', '2'], '2', '5')],
  'stop_time': 5.0},
 {'board': 1.0,
  'depart': 3.0,
  'description': 'route 1 stops 1 2 buses 2 capacity 5\nroad 1 2 4.1\ndisembarks 4.3\n',
  'disembark': 4.2999999999999998,
  'ignore warnings': False,
  'new_passenger': 4.0,
  'optimise parameters': True,
  'roads': [('1', '2', '4.1'), ('2', '1', '1.0')],
  'routes': [('1', ['1', '2'], '2', '5')],
  'stop_time': 5.0},
 {'board': 1.0,
  'depart': 3.0,
  'description': 'route 1 stops 1 2 buses 2 capacity 8\nroad 1 2 1.0\ndisembarks 2.0\n',
  'disembark': 2.0,
  'ignore warnings': False,
  'new_passenger': 4.0,
  'optimise parameters': True,
  'roads': [('1', '2', '1.0'), ('2', '1', '1.0')],
  'routes': [('1', ['1', '2'], '2', '8')],
  'stop_time': 5.0},
 {'board': 1.0,
  'depart': 3.0,
  'description': 'route 1 stops 1 2 buses 2 capacity 8\nroad 1 2 1.0\ndisembarks 8.1\n',
  'disembark': 8.0999999999999996,
  'ignore warnings': False,
  'new_passenger': 4.0,
  'optimise parameters': True,
  'roads': [('1', '2', '1.0'), ('2', '1', '1.0')],
  'routes': [('1', ['1', '2'], '2', '8')],
  'stop_time': 5.0},
 {'board': 1.0,
  'depart': 3.0,
  'description': 'route 1 stops 1 2 buses 2 capacity 8\nroad 1 2 1.0\ndisembarks 4.3\n',
  'disembark': 4.2999999999999998,
  'ignore warnings': False,
  'new_passenger': 4.0,
  'optimise parameters': True,
  'roads': [('1', '2', '1.0'), ('2', '1', '1.0')],
  'routes': [('1', ['1', '2'], '2', '8')],
  'stop_time': 5.0},
 {'board': 1.0,
  'depart': 3.0,
  'description': 'route 1 stops 1 2 buses 2 capacity 8\nroad 1 2 4.1\ndisembarks 2.0\n',
  'disembark': 2.0,
  'ignore warnings': False,
  'new_passenger': 4.0,
  'optimise parameters': True,
  'roads': [('1', '2', '4.1'), ('2', '1', '1.0')],
  'routes': [('1', ['1', '2'], '2', '8')],
  'stop_time': 5.0},
 {'board': 1.0,
  'depart': 3.0,
  'description': 'route 1 stops 1 2 buses 2 capacity 8\nroad 1 2 4.1\ndisembarks 8.1\n',
  'disembark': 8.0999999999999996,
  'ignore warnings': False,
  'new_passenger': 4.0,
  'optimise parameters': True,
  'roads': [('1', '2', '4.1'), ('2', '1', '1.0')],
  'routes': [('1', ['1', '2'], '2', '8')],
  'stop_time': 5.0},
 {'board': 1.0,
  'depart': 3.0,
  'description': 'route 1 stops 1 2 buses 2 capacity 8\nroad 1 2 4.1\ndisembarks 4.3\n',
  'disembark': 4.2999999999999998,
  'ignore warnings': False,
  'new_passenger': 4.0,
  'optimise parameters': True,
  'roads': [('1', '2', '4.1'), ('2', '1', '1.0')],
  'routes': [('1', ['1', '2'], '2', '8')],
  'stop_time': 5.0}]
                simple11_dicts_from_file = parse_input_file('test_files/simple11.text')
                self.assertEqual(simple11, simple11_dicts_from_file)
                
class TestSimulationInstance(unittest.TestCase):
	def setUp(self):
		self.maxDiff = None
		self.test_instance1 = SimulationInstance(parse_input_file('./test_files/simple1.text')[0])
		self.test_instance2 = SimulationInstance(parse_input_file('./test_files/simple2.text')[0])
		self.test_instance3 = SimulationInstance(parse_input_file('./test_files/simple4.text')[0])
        def test_raises(self):
                '''This tests the validation in SimulationInstance and whether it appropriately throws exeptions for errors and warnings'''
                self.assertRaises(SimulationInputError, SimulationInstance ,parse_input_file('test_files/mult_routes')[0])
                self.assertRaises(SimulationInputWarning, SimulationInstance ,parse_input_file('test_files/no_buses')[0])
                self.assertRaises(SimulationInputWarning, SimulationInstance ,parse_input_file('test_files/zero_capacity')[0])
                self.assertRaises(SimulationInputError, SimulationInstance ,parse_input_file('test_files/no_such_stop')[0])
                self.assertRaises(SimulationInputError, SimulationInstance ,parse_input_file('test_files/mult_roads')[0])
                self.assertRaises(SimulationInputWarning, SimulationInstance ,parse_input_file('test_files/no_def_route')[0])
                self.assertRaises(SimulationInputError, SimulationInstance ,parse_input_file('test_files/road_to_self')[0])
                self.assertRaises(SimulationInputError, SimulationInstance ,parse_input_file('test_files/no_cons_stops')[0])
                self.assertRaises(SimulationInputError, SimulationInstance ,parse_input_file('test_files/zero_rate')[0])
                self.assertRaises(SimulationInputError, SimulationInstance ,parse_input_file('test_files/2_zero_control_rates')[0])
                
	def test_proper_encoding(self):
		'''This tests the proper conversion from our input to our internal 'static' simulation representation. The assertions check if the SimulationInstance has the right number of routes/roads and the correct global parameters/flags '''
		self.assertEqual(len(self.test_instance1.routes),1)
		self.assertEqual(self.test_instance1.routes[1].number , 1)
		self.assertEqual(len(self.test_instance1.stops) , 3)
		for i in range(1,4):
			self.assertEqual(self.test_instance1.stops[i].number,i)
		
		self.assertEqual(len(self.test_instance1.buses) , 4)
		capacity = self.test_instance1.buses['1.0'].capacity
		self.assertEqual(capacity,50)
		for i in range(4):
			self.assertEqual(self.test_instance1.buses["1." + str(i)].identifier ,"1." + str(i))
			self.assertEqual(self.test_instance1.buses["1." + str(i)].capacity , capacity )
		self.assertEqual(len(self.test_instance1.roads) ,3)
		
		self.assertEqual(self.test_instance1.board , 1.0)
		self.assertEqual(self.test_instance1.disembarks , 3.0)
		self.assertEqual(self.test_instance1.departs, 3.0)
		self.assertEqual(self.test_instance1.new_passengers , 8.0)
		self.assertEqual(self.test_instance1.stop_time , 100.0)
		self.assertEqual(self.test_instance1.ignore_warnings , False)
		self.assertEqual(self.test_instance1.optimise_parameters ,False)
		'''Below we test similarly for a SimulationInstance obtained from a second file'''
		test_instance2 = self.test_instance2

		self.assertEqual(len(test_instance2.routes) ,2 )
		self.assertEqual(test_instance2.routes[2].number , 2)
		self.assertEqual(test_instance2.routes[3].number , 3)
		self.assertEqual(len(test_instance2.stops) , 5 )
		#Testing routes' stops
		self.assertEqual(set(test_instance2.routes[2].stops) , set([1,2,3]))
		self.assertEqual(set(test_instance2.routes[3].stops) , set([2,4,5]))
		self.assertEqual(set(test_instance2.stops.keys()), set([1,2,3,4,5]))

		for i in range(1,6):
			self.assertEqual(test_instance2.stops[i].number , i )
		self.assertEqual(len(test_instance2.buses) ,11 )
		capacity = test_instance2.buses['2.0'].capacity
		self.assertEqual(capacity , 50)
		for i in range(4):
			self.assertEqual(test_instance2.buses["2." + str(i)].identifier , "2." + str(i))
			self.assertEqual(test_instance2.buses["2." + str(i)].capacity , capacity )
		capacity = test_instance2.buses['3.0'].capacity
		self.assertEqual(capacity , 45 )
		for i in range(7):
			self.assertEqual(test_instance2.buses["3." + str(i)].identifier , "3." + str(i))
			self.assertEqual(test_instance2.buses["3." + str(i)].capacity , capacity )
		self.assertEqual(len(test_instance2.roads) , 6 )
		#this tests whether the roads have the correct first and second stops and rates
		self.assertEqual(test_instance2.roads[0].second_stop.number , 2 )
		self.assertEqual(test_instance2.roads[1].first_stop.number , 2 ) 
		self.assertEqual(test_instance2.roads[1].second_stop.number , 3 ) 
		self.assertEqual(test_instance2.roads[2].first_stop.number , 3 ) 
		self.assertEqual(test_instance2.roads[2].second_stop.number , 1 )
		self.assertEqual(test_instance2.roads[3].first_stop.number , 2 ) 
		self.assertEqual(test_instance2.roads[3].second_stop.number  ,4 ) 
		self.assertEqual(test_instance2.roads[4].first_stop.number , 4) 
		self.assertEqual(test_instance2.roads[4].second_stop.number , 5 ) 
		self.assertEqual(test_instance2.roads[5].first_stop.number , 5) 
		self.assertEqual(test_instance2.roads[5].second_stop.number , 2 )

		self.assertEqual(test_instance2.roads[0].rate , 0.3 )
		self.assertEqual(test_instance2.roads[1].rate , 0.4 )
		self.assertEqual(test_instance2.roads[2].rate , 0.5 )
		self.assertEqual(test_instance2.roads[3].rate , 0.1 )
		self.assertEqual(test_instance2.roads[4].rate , 0.7 )
		self.assertEqual(test_instance2.roads[5].rate , 0.8 )

		# Testing control lines
		self.assertEqual(test_instance2.board , 1.0 )
		self.assertEqual(test_instance2.disembarks , 3.0 )
		self.assertEqual(test_instance2.departs , 3.0 )
		self.assertEqual(test_instance2.new_passengers , 8.0 )
		self.assertEqual(test_instance2.stop_time , 100.0 )
		self.assertEqual(test_instance2.ignore_warnings , False )
		self.assertEqual(test_instance2.optimise_parameters , False )

		'''Below we test similarly for a SimulationInstance obtained from a third file'''
		test_instance3 = self.test_instance3
		self.assertEqual(test_instance3.board , 1.0 )
		self.assertEqual(test_instance3.disembarks , 2.0 )
		self.assertEqual(test_instance3.departs , 3.0 )
		self.assertEqual(test_instance3.new_passengers , 4.0 )
		self.assertEqual(test_instance3.stop_time , 500.0 )
		self.assertEqual(test_instance3.ignore_warnings , True )
		self.assertEqual(test_instance3.optimise_parameters , False )
                
class TestSimulationExecution(unittest.TestCase):
	def setUp(self):
		self.test_execution1 = SimulationExecution(SimulationInstance(parse_input_file('./test_files/simple1.text')[0]),print_events=False)
		self.test_execution3 = SimulationExecution(SimulationInstance(parse_input_file('./test_files/simple3.text')[0]),print_events=False)
		self.test_execution4 = SimulationExecution(SimulationInstance(parse_input_file('./test_files/simple4.text')[0]),print_events=False)
		self.test_execution5 = SimulationExecution(SimulationInstance(parse_input_file('./test_files/simple5.text')[0]),print_events=False)
                self.test_execution6 = SimulationExecution(SimulationInstance(parse_input_file('./test_files/simple6.text')[0]),print_events=False)
		self.simple1_buses = [
		"Identifier: 1.0\nRoute number: 1\nCapacity: 50\nState: ('stop', 1)\nPassengers: set([])",
		"Identifier: 1.1\nRoute number: 1\nCapacity: 50\nState: ('stop', 2)\nPassengers: set([])",
		"Identifier: 1.2\nRoute number: 1\nCapacity: 50\nState: ('stop', 3)\nPassengers: set([])",
		"Identifier: 1.3\nRoute number: 1\nCapacity: 50\nState: ('stop', 1)\nPassengers: set([])"]
		self.simple3_buses = [
		"Identifier: 2.0\nRoute number: 2\nCapacity: 50\nState: ('stop', 1)\nPassengers: set([])",
		"Identifier: 2.1\nRoute number: 2\nCapacity: 50\nState: ('stop', 2)\nPassengers: set([])",
		"Identifier: 2.2\nRoute number: 2\nCapacity: 50\nState: ('stop', 3)\nPassengers: set([])",
		"Identifier: 2.3\nRoute number: 2\nCapacity: 50\nState: ('stop', 1)\nPassengers: set([])",
		"Identifier: 3.0\nRoute number: 3\nCapacity: 45\nState: ('stop', 2)\nPassengers: set([])",
		"Identifier: 3.1\nRoute number: 3\nCapacity: 45\nState: ('stop', 4)\nPassengers: set([])",
		"Identifier: 3.2\nRoute number: 3\nCapacity: 45\nState: ('stop', 5)\nPassengers: set([])",
		"Identifier: 3.3\nRoute number: 3\nCapacity: 45\nState: ('stop', 2)\nPassengers: set([])",
		"Identifier: 3.4\nRoute number: 3\nCapacity: 45\nState: ('stop', 4)\nPassengers: set([])",
		"Identifier: 3.5\nRoute number: 3\nCapacity: 45\nState: ('stop', 5)\nPassengers: set([])",
		"Identifier: 3.6\nRoute number: 3\nCapacity: 45\nState: ('stop', 2)\nPassengers: set([])",]
                
	def test_create_passenger(self):
		'''Tests whether the create_passenger method spawns passengers with a destination different from their origin, whether the state is set to the origin stop, and whether the origin stop contains the destination route, which is equivalent to the destination stop being on the same route as the origin stop'''
		for i in range(100000): 
			cur_pas1 = self.test_execution1.create_passenger()
			cur_pas3 = self.test_execution3.create_passenger()
			cur_pas4 = self.test_execution4.create_passenger()
			self.assertTrue(cur_pas1.origin_stop != cur_pas1.destination_stop)
			self.assertTrue(cur_pas3.origin_stop != cur_pas3.destination_stop)
			self.assertTrue(cur_pas4.origin_stop != cur_pas4.destination_stop)
			self.assertEqual(cur_pas1.state, ('stop',cur_pas1.origin_stop))
			self.assertEqual(cur_pas3.state, ('stop',cur_pas3.origin_stop))
			self.assertEqual(cur_pas4.state, ('stop',cur_pas4.origin_stop))
			self.assertTrue(cur_pas1.destination_route in self.test_execution1.simulation_instance.stops[cur_pas1.origin_stop].routes)
			self.assertTrue(cur_pas3.destination_route in self.test_execution3.simulation_instance.stops[cur_pas3.origin_stop].routes)
			self.assertTrue(cur_pas4.destination_route in self.test_execution4.simulation_instance.stops[cur_pas4.origin_stop].routes)
	def test_position_buses(self):
		'''Test whether buses are positioned appropriated on their queue. NOTE: this test depends on the order of which we have put the buses i.e. if two routes a and b share a stop c, then the buses on road a will appear first in the queue if road a is processed before road b'''
		self.test_execution1.position_buses()
		self.test_execution3.position_buses()
		self.test_execution4.position_buses()
                for _bus,reg_bus in zip(sorted(self.test_execution1.simulation_instance.buses.values(), key=lambda bus: bus.identifier),self.simple1_buses):
			self.assertEqual(str(_bus), str(reg_bus))

		for _bus,reg_bus in zip(sorted(self.test_execution3.simulation_instance.buses.values(), key=lambda bus: bus.identifier),self.simple3_buses):
			self.assertEqual(str(_bus), str(reg_bus))

		
		self.assertEqual(self.test_execution1.simulation_instance.stops[1].bus_queue, deque(['1.0','1.3']))
		self.assertEqual(self.test_execution1.simulation_instance.stops[2].bus_queue, deque(['1.1']))
		self.assertEqual(self.test_execution1.simulation_instance.stops[3].bus_queue, deque(['1.2']))

		self.assertEqual(self.test_execution3.simulation_instance.stops[1].bus_queue, deque(['2.0','2.3']))
		self.assertEqual(self.test_execution3.simulation_instance.stops[2].bus_queue, deque(['2.1','3.0','3.3','3.6']))
		self.assertEqual(self.test_execution3.simulation_instance.stops[3].bus_queue, deque(['2.2']))
		self.assertEqual(self.test_execution3.simulation_instance.stops[4].bus_queue, deque(['3.1','3.4']))
		self.assertEqual(self.test_execution3.simulation_instance.stops[5].bus_queue, deque(['3.2','3.5']))

	def test_normalise_and_unzip(self):
		'''Tests normalise and unzip functions used in the simulation loop'''
		self.assertEqual(unzip([(1,2),(3,4),(5,6)]),[[1,3,5],[2,4,6]])
		norm = normalise([1,2,0.1])
		self.assertTrue(norm[0] - 0.32258064516129031 < 0.0001 and norm[0] - 0.32258064516129031 >= 0.0)
		self.assertTrue(norm[1] - 0.64516129032258063 < 0.0001 and norm[1] - 0.64516129032258063 >= 0.0)
		self.assertTrue(norm[2] - 0.032258064516129031 < 0.0001 and norm[2] - 0.032258064516129031 >= 0.0)
	def test_run_simulation(self):
		'''This method runs the simulation on simple[1,3,4,5].text and then performs a series of assertion tests on the final state of the simulation by calling post_simulation_sanity() on the the finished executions'''
		self.test_execution1.position_buses()
		self.test_execution1.run_simulation()
                self.test_execution1.post_simulation_sanity()
                
                self.test_execution3.position_buses()
		self.test_execution3.run_simulation()
                self.test_execution3.post_simulation_sanity()

                self.test_execution4.position_buses()
		self.test_execution4.run_simulation()
                self.test_execution4.post_simulation_sanity()

                self.test_execution5.position_buses()
                self.test_execution5.run_simulation()
                self.test_execution5.post_simulation_sanity()
        def test_run_simulation_analysis_with_seed(self):
                '''This method runs the simulation on simple9.text with a predetermined seed of 7883 and regresses the event output and the number of missed passengers and average passengers on buses/routes/overall. It also runs assertion tests (post_simulation_sanity) on the final state of the simulation'''
                test_execution9_c1 = SimulationExecution(SimulationInstance(parse_input_file('./test_files/simple9.text')[0]),print_events=False,seed=7883,count_missed_passengers_multiple_times=False) #counting missed passengers once only case
                simple9_c1_output ='''A new passenger enters at stop 2 with destination 1 at time 1.71795651916
Passenger boards bus 1.1 at stop 2 with destination 1 at time 1.92520124041
A new passenger enters at stop 2 with destination 1 at time 2.81832645893
Bus 1.0 leaves stop 1 at time 6.31864969272
Bus 1.0 arrives at stop 2 at time 6.38369454147
A new passenger enters at stop 1 with destination 2 at time 15.7089285311
A new passenger enters at stop 2 with destination 1 at time 17.3160491963
A new passenger enters at stop 2 with destination 1 at time 24.8094421461
Bus 1.1 leaves stop 2 at time 27.8375150684
Passenger boards bus 1.0 at stop 2 with destination 1 at time 27.8974452434
Bus 1.1 arrives at stop 1 at time 28.2063545858
Passenger disembarks bus 1.1 at stop 1 at time 28.6773946012
Passenger boards bus 1.1 at stop 1 with destination 2 at time 28.8932474766
Bus 1.0 leaves stop 2 at time 28.9223109125
Bus 1.0 arrives at stop 1 at time 29.9106013251
Passenger disembarks bus 1.0 at stop 1 at time 30.9404345365
A new passenger enters at stop 2 with destination 1 at time 36.0226942058
number of missed passengers stop 1 0
number of missed passengers stop 2 3
number of missed passengers route 1 3
number of missed passengers 3
average passengers bus 1.0 0.5
average passengers bus 1.1 1.0
average passengers route 1 0.666666666667
average passengers 0.666666666667
average queueing at stop 1 3.05604644037
average queueing at stop 2 21.4538205269
average queueing at all stops 5.51318268153
average waiting passengers at stop 1 0.366000357167
average waiting passengers at stop 2 1.53254113253
average waiting passengers on route 1 1.8985414897
average waiting passengers 1.8985414897
'''
                simple9_c1_buses_dict = {'overall': [3, 2], 1: [3, 2], '1.0': [2, 1], '1.1': [1, 1]}
                test_execution9_c2 = SimulationExecution(SimulationInstance(parse_input_file('./test_files/simple9.text')[0]),print_events=False,seed=7883,count_missed_passengers_multiple_times=True) #counting missed passengers more than once case                
                simple9_c2_output = '''A new passenger enters at stop 2 with destination 1 at time 1.71795651916
Passenger boards bus 1.1 at stop 2 with destination 1 at time 1.92520124041
A new passenger enters at stop 2 with destination 1 at time 2.81832645893
Bus 1.0 leaves stop 1 at time 6.31864969272
Bus 1.0 arrives at stop 2 at time 6.38369454147
A new passenger enters at stop 1 with destination 2 at time 15.7089285311
A new passenger enters at stop 2 with destination 1 at time 17.3160491963
A new passenger enters at stop 2 with destination 1 at time 24.8094421461
Bus 1.1 leaves stop 2 at time 27.8375150684
Passenger boards bus 1.0 at stop 2 with destination 1 at time 27.8974452434
Bus 1.1 arrives at stop 1 at time 28.2063545858
Passenger disembarks bus 1.1 at stop 1 at time 28.6773946012
Passenger boards bus 1.1 at stop 1 with destination 2 at time 28.8932474766
Bus 1.0 leaves stop 2 at time 28.9223109125
Bus 1.0 arrives at stop 1 at time 29.9106013251
Passenger disembarks bus 1.0 at stop 1 at time 30.9404345365
A new passenger enters at stop 2 with destination 1 at time 36.0226942058
number of missed passengers stop 1 0
number of missed passengers stop 2 5
number of missed passengers route 1 5
number of missed passengers 5
average passengers bus 1.0 0.5
average passengers bus 1.1 1.0
average passengers route 1 0.666666666667
average passengers 0.666666666667
average queueing at stop 1 3.05604644037
average queueing at stop 2 21.4538205269
average queueing at all stops 5.51318268153
average waiting passengers at stop 1 0.366000357167
average waiting passengers at stop 2 1.53254113253
average waiting passengers on route 1 1.8985414897
average waiting passengers 1.8985414897
'''
                test_execution9_c3 = SimulationExecution(SimulationInstance(parse_input_file('./test_files/simple9.text')[0]),print_events=False,seed=710,count_missed_passengers_multiple_times=False)
                simple9_c3_output = '''Bus 1.0 leaves stop 1 at time 3.53525546758
Bus 1.0 arrives at stop 2 at time 4.19210421068
A new passenger enters at stop 2 with destination 1 at time 4.36028255
Passenger boards bus 1.1 at stop 2 with destination 1 at time 4.47581902481
Bus 1.1 leaves stop 2 at time 6.47017070877
Bus 1.1 arrives at stop 1 at time 7.68442473281
Passenger disembarks bus 1.1 at stop 1 at time 7.69584112618
A new passenger enters at stop 2 with destination 1 at time 12.4688354263
Passenger boards bus 1.0 at stop 2 with destination 1 at time 12.5102106454
Bus 1.0 leaves stop 2 at time 17.1982905531
Bus 1.0 arrives at stop 1 at time 17.5464450687
Passenger disembarks bus 1.0 at stop 1 at time 17.5793728314
Bus 1.0 leaves stop 1 at time 19.9146031283
Bus 1.0 arrives at stop 2 at time 20.7598293214
Bus 1.0 leaves stop 2 at time 21.6501957497
Bus 1.0 arrives at stop 1 at time 21.7874005012
Bus 1.1 leaves stop 1 at time 25.5298391957
Bus 1.1 arrives at stop 2 at time 26.6458252127
A new passenger enters at stop 2 with destination 1 at time 27.1900720544
Passenger boards bus 1.1 at stop 2 with destination 1 at time 27.2803086826
A new passenger enters at stop 2 with destination 1 at time 31.1144149607
Bus 1.0 leaves stop 1 at time 34.5694998287
Bus 1.1 leaves stop 2 at time 34.8930809675
Bus 1.0 arrives at stop 2 at time 35.6154824774
Passenger boards bus 1.0 at stop 2 with destination 1 at time 35.6700885655
Bus 1.1 arrives at stop 1 at time 36.836973586
Bus 1.0 leaves stop 2 at time 37.183780656
Passenger disembarks bus 1.1 at stop 1 at time 37.3855643013
Bus 1.1 leaves stop 1 at time 38.5939927913
Bus 1.1 arrives at stop 2 at time 39.1974641581
Bus 1.0 arrives at stop 1 at time 41.8392041797
Passenger disembarks bus 1.0 at stop 1 at time 43.6083166646
A new passenger enters at stop 1 with destination 2 at time 44.8880885949
Passenger boards bus 1.0 at stop 1 with destination 2 at time 44.9941686933
number of missed passengers stop 1 0
number of missed passengers stop 2 1
number of missed passengers route 1 1
number of missed passengers 1
average passengers bus 1.0 0.333333333333
average passengers bus 1.1 0.5
average passengers route 1 0.4
average passengers 0.4
average queueing at stop 1 1.52764918851
average queueing at stop 2 0.759355499361
average queueing at all stops 0.69905527101
average waiting passengers at stop 1 0.00235764103338
average waiting passengers at stop 2 0.106743208431
average waiting passengers on route 1 0.109100849464
average waiting passengers 0.109100849464
'''
                file1 = open('test_files/seed7883','r')
                state1_2 = eval(file1.read())
                file2 = open('test_files/seed710','r')
                state3 = eval(file2.read())
                file1.close()
                file2.close()
                
                
                ##
                test_execution9_c1.random_inst.setstate(state1_2)
                test_execution9_c1.position_buses()
                output1 = test_execution9_c1.run_simulation()
                self.assertEqual(output1 + test_execution9_c1.get_statistics() ,simple9_c1_output)
                self.assertEqual(test_execution9_c1.buses_journeys_dict,simple9_c1_buses_dict) #in case they are only numerically equal
                test_execution9_c1.post_simulation_sanity()
                
                test_execution9_c2.random_inst.setstate(state1_2)
                test_execution9_c2.position_buses()
                output2 = test_execution9_c2.run_simulation()
                self.assertEqual(output2 + test_execution9_c2.get_statistics() ,simple9_c2_output)
                self.assertEqual(test_execution9_c2.buses_journeys_dict,simple9_c1_buses_dict) #in case they are only numerically equal
                test_execution9_c2.post_simulation_sanity()
                                
                test_execution9_c3.random_inst.setstate(state3)
                test_execution9_c3.position_buses()
                output3 = test_execution9_c3.run_simulation()
                self.assertEqual(output3 + test_execution9_c3.get_statistics(),simple9_c3_output)
                test_execution9_c3.post_simulation_sanity()

	def test_bus_add_passenger(self):
		'''This tests whether the add passenger method for Buses handles them appropriately'''
		test_bus = Bus()
		test_bus.set_capacity(20)
		test_bus.add_passenger('1')
		test_bus.add_passenger('2')
		test_bus.add_passenger('3')
		test_bus.add_passenger('4')
		self.assertEqual(len(test_bus._passengers), 4)

	def test_passenger_creation(self):
		'''This tests whether passenger creation is handled appropriately and also whether the rest_global function resets the global passenger identifier counter properly'''
		Passenger.reset_global_counter()
		test_passenger0 = Passenger()
		test_passenger1 = Passenger()
		self.assertEqual(test_passenger0.identifier , 0)
		self.assertEqual(test_passenger1.identifier , 1)
		Passenger.reset_global_counter()

        def test_discrete_dist_sample(self):
                '''This tests discrete distribution sampling. Because sampling is stochastic, we cannot simply assert any properties and thus we try and catch if an exception occurs and report that to the user.'''
                d = {'a':0,'b':0,'c':0}
                values = array(['a','b','c'])
                probabilities = array([0.1,0.3,0.6])
                for i in range(100000):
                        d[self.test_execution1.discrete_dist_sample(values,probabilities)] += 1
                try:
                        self.assertTrue(fabs(d['a']/100000.0 - 0.1) < 1.0e-2)
                        self.assertTrue(fabs(d['b']/100000.0 - 0.3) < 1.0e-2)
                        self.assertTrue(fabs(d['c']/100000.0 - 0.6) < 1.0e-2)
                except AssertionError:
                        print "WARNING: \n Stochastic test for discrete_dist_sample failed. You are STRONGLY advised to re-run the tests and if this persists, to investigate further\n WARNING"
        def test_compute_statistics(self):
                '''This tests whether compute_statistics correctly computes the average numbers of events from a given output file. Currently average number of passengers entering and passengers boarding events are supported'''
                (num_passengers,num_boards,num_disembarks,num_departs,num_arrives) = compute_statistics(2,file_name='./test_files/stats.text')
                self.assertEqual(num_passengers,2.0)
                self.assertTrue(fabs(num_boards-1.5) < 1e-4)
                str = '''Bus 1.0 leaves stop 1 at time 0.0
A new passenger enters at stop 2 with destination 1 at time 1.47698014925
Passenger boards bus 1.1 at stop 2 with destination 1 at time 2.46808971894
Bus 1.0 arrives at stop 2 at time 2.62414814565
A new passenger enters at stop 1 with destination 2 at time 3.15711843795
Bus 1.1 leaves stop 2 at time 6.35344577415
Bus 1.1 arrives at stop 1 at time 10.6330000625
Passenger boards bus 1.1 at stop 1 with destination 2 at time 11.143733069
Passenger disembarks bus 1.1 at stop 1 at time 11.1750933051
Bus 1.0 leaves stop 2 at time 12.0364347324
Bus 1.0 arrives at stop 1 at time 13.8501855739
Bus 1.1 leaves stop 1 at time 14.3228272534
Bus 1.1 arrives at stop 2 at time 16.2251501931
Bus 1.0 leaves stop 1 at time 17.0476270943
Bus 1.0 arrives at stop 2 at time 18.2973161933
Passenger disembarks bus 1.1 at stop 2 at time 18.9819807895'''
                (num_passengers,num_boards,num_disembarks,num_departs,num_arrives) = compute_statistics(1,string=str)
                self.assertEqual(num_passengers,2.0)
                self.assertEqual(num_boards,2.0)
                self.assertEqual(num_disembarks,2.0)
                self.assertEqual(num_departs,5.0)
                self.assertEqual(num_arrives,5.0)
                        
        def test_passengers_waiting_for_bus(self):
                '''This tests whether passengers on a stop that want to go to a particular destination want to get on the bus that goes in that particular destination'''
                Passenger.reset_global_counter() # so that we don't interfere with other tests
                # we create a passenger, pas1, who is waiting for a bus to take him to stop 2 along route 1
                pas1 = Passenger()
                pas1.set_origin_stop(1)
                pas1.set_destination_stop(2)
                pas1.set_state(('stop',1))
                pas1.set_destination_route(1)
                self.test_execution6.passengers.add(pas1)
                self.test_execution6.simulation_instance.stops[1].passenger_set.add(pas1)
                # we first position the buses along the stops
                self.test_execution6.position_buses()
                # there should be passengers waiting for bus 1.0 at stop 1
                self.assertTrue(self.test_execution6.simulation_instance.stops[1].are_passengers_waiting_for_bus(self.test_execution6.simulation_instance.buses['1.0']))
                # there should be no passenger waiting for bus 1.1 at stop 2
                self.assertTrue(not self.test_execution6.simulation_instance.stops[2].are_passengers_waiting_for_bus(self.test_execution6.simulation_instance.buses['1.1']))
                self.test_execution6.simulation_instance.buses['1.0'].set_state(('road',('1','2')))
                # re-read simulation execution from file so that if another test uses it everything is back to default setup
                Passenger.reset_global_counter() # so that we don't interfere with other tests
                self.test_execution6 = SimulationExecution(SimulationInstance(parse_input_file('./test_files/simple6.text')[0]))

        def test_passenger_waiting_to_disembark(self):
                '''This tests whether passengers on a bus want to disembark on their destination stops if the bus they are on is on that stop'''
                Passenger.reset_global_counter() # so that we don't interfere with other tests
                self.test_execution6.position_buses()
                pas1 = Passenger()
                pas1.set_origin_stop(1)
                pas1.set_destination_stop(2)
                pas1.set_state(('bus','1.1'))
                pas1.set_destination_route(1)
                self.test_execution6.passengers.add(pas1)
                self.test_execution6.simulation_instance.buses['1.1']._passengers.add(pas1)
                
                # pas1 is on bus 1.1, which is on stop 2, and thus pas1 would like to disembark from the bus
                self.assertTrue(self.test_execution6.simulation_instance.buses['1.1'].are_passengers_waiting_to_disembark_cur_stop())
                # testing throwing when bus is on a road
                self.test_execution6.simulation_instance.buses['1.0'].set_state(('road',('1','2')))
        
        def test_get_delay(self):
                '''Tests whether the get_delay function successfully samples from the exponential distribution'''
                # By maximimum likelihood, the rate parameter lamba_hat is estimated as 1/x_bar where x_bar
                # is the sample mean of the obtained values
                # we test for different rates, inreasing them geometrically
                rate1 = 0.01
                xs1 = []
                n1 = 100000
                for i in range(n1):
                        xs1.append(self.test_execution1.get_delay(rate1))
                lambda1_hat = n1 / sum(xs1) # 1 / x_hat ; x_hat = SUM(xs)/n
                # the maximum-likelihood estimate of the rate parameter of the exponential distribution 
                # that get_delay samples from should be approximately equal to the original rate
                self.assertTrue(fabs(rate1 - lambda1_hat) < 1e-3)
                # we mustn't get any negative values
                self.assertTrue(len([x for x in xs1 if x < 0.0])==0.0)
                
                rate2 = 0.1
                xs2 = []
                n2 = 100000
                for i in range(n2):
                        xs2.append(self.test_execution1.get_delay(rate2))
                lambda2_hat = n2 / sum(xs2) # 1 / x_hat ; x_hat = SUM(xs)/n
                # the maximum-likelihood estimate of the rate parameter of the exponential distribution 
                # that get_delay samples from should be approximately equal to the original rate
                self.assertTrue(fabs(rate2 - lambda2_hat) < 1e-2)
                # we mustn't get any negative values
                self.assertTrue(len([x for x in xs2 if x < 0.0])==0.0)
                
                rate3 = 1
                xs3 = []
                n3 = 100000
                for i in range(n3):
                        xs3.append(self.test_execution1.get_delay(rate3))
                lambda3_hat = n3 / sum(xs3) # 1 / x_hat ; x_hat = SUM(xs)/n
                # the maximum-likelihood estimate of the rate parameter of the exponential distribution 
                # that get_delay samples from should be approximately equal to the original rate
                self.assertTrue(fabs(rate3 - lambda3_hat) < 1e-1)
                # we mustn't get any negative values
                self.assertTrue(len([x for x in xs3 if x < 0.0])==0.0)
                
                rate4 = 10
                xs4 = []
                n4 = 100000
                for i in range(n4):
                        xs4.append(self.test_execution1.get_delay(rate4))
                lambda4_hat = n4 / sum(xs4) # 1 / x_hat ; x_hat = SUM(xs)/n
                # the maximum-likelihood estimate of the rate parameter of the exponential distribution 
                # that get_delay samples from should be approximately equal to the original rate

                self.assertTrue(fabs(rate4 - lambda4_hat) < 1e-1)
                # we mustn't get any negative values
                self.assertTrue(len([x for x in xs4 if x < 0.0])==0.0)

        def test_params_opt(self):
                '''This tests parameter optimisation assertions'''
                
        
