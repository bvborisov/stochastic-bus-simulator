try:
    import ipdb
except ImportError:
    import pdb

from simulator_io import parse_input_file
from simulation_execution import *
from simulation_instance import *

class SimulationCollection():
    def __init__(self,input_file_name,seed=None,disembark_before_board=False,count_missed_passengers_multiple_times=False,print_events=True,output_file_name=None,post_simulation_validation=False):
        self.parsed_dicts = parse_input_file(input_file_name) # get all dictionaries from file
        self.simulation_executions = []
        if len(self.parsed_dicts) > 1:
            print_events = False
        self.print_events = print_events
        for d in self.parsed_dicts: # append all possible simulation instaces
            cur_exec = SimulationExecution(SimulationInstance(d),seed,disembark_before_board,count_missed_passengers_multiple_times,self.print_events)
            self.simulation_executions.append(cur_exec)
        self.output_string = ''
        self.output_to_file = True
        if output_file_name == None:
            self.output_to_file = False
        self.output_file_name = output_file_name
        self.full_output_string = ''
        self.post_simulation_validation = post_simulation_validation
    def run_all_simulations(self):
        '''This method runs all simulations on all simulation executions'''
        cost_sim_list = []
        for sim in self.simulation_executions:
            sim.position_buses()
            sim_output = sim.run_simulation()
            if self.post_simulation_validation:
                sim.post_simulation_sanity()
            sim_stats = sim.get_statistics()
            sim_cost = sim.get_cost()
            
            if self.print_events:
                self.output_string += sim_output + sim_stats + "\n"
            else:
                self.output_string += sim.simulation_instance.description + sim_stats + "\n"
            # full outputstring always gets updated
            if self.output_to_file:
                self.full_output_string += sim.simulation_instance.description + sim_output + sim_stats + "\n"
            opt_params = ''
            if sim.simulation_instance.optimise_parameters:
                cost_sim_list.append((sim_cost,sim.simulation_instance.description))
        if len(cost_sim_list) > 0:
            cost_sim_list.sort()
            cost_descr = cost_sim_list[0]
            self.output_string += "Parameter Optimisation:\n=============================\nThe lowest cost was " + str(cost_descr[0]) + " and was obtained for settings: \n" + cost_descr[1] 
            self.full_output_string += "Parameter Optimisation:\n=============================\nThe lowest cost was " + str(cost_descr[0]) + " and was obtained for settings: \n" + cost_descr[1] 
        return self.output_string
    def write_to_file(self):
        if self.output_to_file:
            f = open(self.output_file_name,'w')
            f.write(self.full_output_string)
            f.close()
        return
