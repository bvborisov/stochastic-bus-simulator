import string
import re
from itertools import product
import time,sys
try:
    import ipdb
except ImportError:
    import pdb

def parse_input_file(file_name):
    error_flag = 0
    warning_flag = 0
    error_str = ''
    warning_str = ''
    file_handle = open(file_name, 'r')
    file_str = file_handle.read()
    file_handle.close()
    # regular expressions for all different types of control lines below - quite cryptic, I apologise
    route_re = re.compile(r"^\s*route\s+(?P<route>\d+)\s+stops(?P<stops>(\s+\d+)+)\s+buses\s+(?P<buses>(?:(?:experiment)(?:\s+\d+)+)|(?:\d+))\s+capacity\s+(?P<capacity>(?:(?:experiment)(?:\s+\d+)+)|(?:\d+))(?:(?:\s*$)|(?:\s*?#.*?$))", re.MULTILINE)
    road_re = re.compile(r"^\s*road\s+(?P<origin>\d+)\s+(?P<destination>\d+)\s+(?P<rate>(?:(?:experiment)(?:(?:\s+\d+\.\d+)|(?:\s+\d+))+)|(?:\d+\.\d+)|(?:\d+))(?:(?:\s*$)|(?:\s*?#.*?$))", re.MULTILINE)
        
    board_re = re.compile(r"^\s*board\s+(?P<rate>(?:(?:experiment)(?:(?:\s+\d+\.\d+)|(?:\s+\d+))+)|(?:\d+\.\d+)|(?:\d+))(?:(?:\s*$)|(?:\s*?#.*?$))", re.MULTILINE)
    disembark_re = re.compile(r"^\s*disembarks\s+(?P<rate>(?:(?:experiment)(?:(?:\s+\d+\.\d+)|(?:\s+\d+))+)|(?:\d+\.\d+)|(?:\d+))(?:(?:\s*$)|(?:\s*?#.*?$))", re.MULTILINE)
    departs_re = re.compile(r"^\s*departs\s+(?P<rate>(?:(?:experiment)(?:(?:\s+\d+\.\d+)|(?:\s+\d+))+)|(?:\d+\.\d+)|(?:\d+))(?:(?:\s*$)|(?:\s*?#.*?$))", re.MULTILINE)
    new_passengers_re = re.compile(r"^\s*new\s+passengers\s+(?P<rate>(?:(?:experiment)(?:(?:\s+\d+\.\d+)|(?:\s+\d+))+)|(?:\d+\.\d+)|(?:\d+))(?:(?:\s*$)|(?:\s*?#.*?$))", re.MULTILINE)
    stop_time_re = re.compile(r"^\s*stop\s+time\s+(?P<time>(?:\d+\.\d+)|(?:\d+))(?:(?:\s*$)|(?:\s*?#.*?$))", re.MULTILINE)
	
    ignore_warnings_re = re.compile(r"^\s*ignore\s+warnings(?:(?:\s*$)|(?:\s*?#.*?$))", re.MULTILINE)
    optimise_parameters_re = re.compile(r"^\s*optimise\s+parameters(?:(?:\s*$)|(?:\s*?#.*?$))", re.MULTILINE)
    # contain matches for all regular expressions - we create the dicionary
    # from them
    route_matches = route_re.findall(file_str)
    road_matches = road_re.findall(file_str)
    board_matches = board_re.findall(file_str)
    disembark_matches = disembark_re.findall(file_str)
    departs_matches = departs_re.findall(file_str)
    new_passengers_matches = new_passengers_re.findall(file_str)
    stop_time_matches = stop_time_re.findall(file_str)
    ignore_warnings_matches = ignore_warnings_re.findall(file_str)
    optimise_parameters_matches = optimise_parameters_re.findall(file_str)

    #1st level of Validation: very coarse
    if route_matches == []:
        error_str += "Error: No routes defined\n"
        error_flag = 1
    if road_matches == []:
        error_str += "Error: No roads defined\n"
        error_flag = 1
    if len(board_matches) > 1:
        error_str += "Error: More than one board line\n"
        error_flag = 1
    if len(board_matches) == 0:
        error_str += "Error: Board line is not present in the input file\n"
        error_flag = 1
    if len(disembark_matches) > 1:
        error_str += "Error: More than one disembarks line\n"
        error_flag = 1
    if len(disembark_matches) == 0:
        error_str += "Error: Disembarks line is not present in the input file\n"
        error_flag = 1
    if len(departs_matches) > 1:
        error_str += "Error: More than one departs line\n"
        error_flag = 1
    if len(departs_matches) == 0:
        error_str += "Error: Departs line is not present in the input file\n"
        error_flag = 1
    if len(new_passengers_matches) > 1:
        error_str += "Error: More than one new passengers line"
        error_flag = 1
    if len(new_passengers_matches) == 0:
        error_str += "Error: New Passengers line is not present in the input file\n"
        error_flag = 1
    if len(stop_time_matches) > 1:
        error_str += "Error: More than one stop time line\n"
        error_flag = 1
    if len(stop_time_matches) == 0:
        error_str += "Error: Stop time line is not present in the input file\n"
        error_flag = 1
    if len(ignore_warnings_matches) > 1:
        warning_str += "Warning: Multiple ignore warnings lines\n"
        warning_flag = 1
    if len(optimise_parameters_matches) > 1:
        warning_str += "Warning: Multiple parameters lines lines\n"
        warning_flag = 1
    if len(optimise_parameters_matches) == 1 :
        exp_present = False
        #checking if we have experiments since we have optimise parameters
        for route in route_matches:
            if 'experiment' in route[3]:
                exp_present = True
                break
            if 'experiment' in route[4]:
                exp_present = True
                break
        for road in road_matches:
            if 'experiment' in road[2]:
                exp_present = True
                break
        if 'experiment' in board_matches[0]:
            exp_present = True
        if 'experiment' in disembark_matches[0]:
            exp_present = True
        if 'experiment' in departs_matches[0]:
            exp_present = True
        if 'experiment' in new_passengers_matches[0]:
            exp_present = True
        if not exp_present:
            error_str += "Error: Optimise parameters control line present but no experiments present\n"
            error_flag = 1
    if error_flag == 1:
        raise SimulationInputError(error_str)
    if warning_flag == 1 and len(ignore_warnings_matches) == 1:
        raise SimulationInputWarning(warning_str)
        return
    # helper functions for dealing with the matched experiment word
    def split_stops(x):
        return (x[0],x[1].split(' ')[1:],x[3],x[4])
    def split_exp1(x): #for routes
        return (x[0],x[1],x[2].replace('experiment','').split(),x[3].replace('experiment','').split())
    def split_exp2(x): #for roads
        return (x[0],x[1],x[2].replace('experiment','').split())
    def split_exp3(x): #for board/departs/...
        return x[0].replace('experiment','').split()
    # removing experiment word
    route_matches = map(lambda x: split_stops(x), route_matches)
    route_matches = map(lambda x: split_exp1(x), route_matches)
    road_matches = map(lambda x: split_exp2(x), road_matches)
    board_matches = split_exp3(board_matches)
    disembark_matches = split_exp3(disembark_matches)
    departs_matches = split_exp3(departs_matches)
    new_passengers_matches = split_exp3(new_passengers_matches)

    #populating dicts

    dicts_size = 1 # we need to know size in advance - it is product of all experiments list lengths
    product_list = [] # list of all lists than contain different experimentation values

    for route in route_matches:
        dicts_size *= len(route[2]) # stripping of keyword and making into a list
        dicts_size *= len(route[3])
        product_list.append(route[2])
        product_list.append(route[3])
    for road in road_matches:
        dicts_size *= len(road[2]) # stripping of keyword and making into a list
        product_list.append(road[2])
        
    dicts_size *= len(board_matches) * len(disembark_matches) * len(departs_matches) * len(new_passengers_matches)
    product_list.append(board_matches)
    product_list.append(disembark_matches)
    product_list.append(departs_matches)
    product_list.append(new_passengers_matches)
    # this computes the cartesian product of all lists in product_list and gives back a list of combination tuples
    product_tuples = product(*product_list) 

    stop_time = float(stop_time_matches[0])
    ignore_warnings = len(ignore_warnings_matches) >= 1 #two ignore_warnings line are not enough for
    optimise_params = len(optimise_parameters_matches) >= 1
    routes_without_exp_buses_capacity = [[x[0],x[1],'',''] for x in route_matches] # routes with 'holes' that will be populated
    roads_without_exp_rates = [[x[0],x[1],''] for x in road_matches] # roads with 'hoels' that will be populated
    dicts = [
        {'board':None,
         'depart':None,
         'disembark':None,
         'ignore warnings':ignore_warnings,
         'new_passenger':None,
         'optimise parameters':optimise_params,
         'roads':roads_without_exp_rates, 
         'routes':routes_without_exp_buses_capacity,
         'stop_time':stop_time,
         'description':''} for x in range(dicts_size)] # we create all dictionary 'molds' here and we populate them below
    i = 0 #index in dicts
    j = 0 #index in tup
    num_routes = len(routes_without_exp_buses_capacity)
    num_roads = len(roads_without_exp_rates)
    k = 0 #index for routes
    # populates all dicionaries in dicts with the appropriate combinations of experimentation values
    for tup in product_tuples:
        #iteration over the order of routes as they appear in the file
        description_str = '' # this is the string representation of the current experimentation setting
        for route in dicts[i]['routes']:
            route[2] = tup[j]
            j += 1
            route[3] = tup[j]
            j += 1
            if len(product_list[k]) > 1 or len(product_list[k+1]) > 1:
                description_str += 'route ' + str(route[0]) + ' stops '
                for stop in route[1]:
                    description_str += str(stop) + ' '
                description_str += 'buses ' + str(route[2]) + ' capacity ' + route[3] + '\n'
            k += 2
        dicts[i]['routes'] = map(tuple, dicts[i]['routes'])
        for road in dicts[i]['roads']:
            road[2] = tup[j]
            j += 1
            if len(product_list[k]) > 1:
                description_str += 'road ' + str(road[0]) + ' ' + str(road[1]) + ' ' + str(road[2]) + '\n'
            k += 1
        dicts[i]['roads'] = map(tuple, dicts[i]['roads'])
        dicts[i]['board'] = float(tup[j])
        if len(product_list[k]) > 1:
            description_str += 'board ' + str(dicts[i]['board']) + '\n'
        j += 1
        k += 1
        #
        dicts[i]['disembark'] = float(tup[j])
        if len(product_list[k]) > 1:
            description_str += 'disembarks ' + str(dicts[i]['disembark']) + '\n'
        j += 1
        k += 1
        #
        dicts[i]['depart'] = float(tup[j])
        if len(product_list[k]) > 1:
            description_str += 'departs ' + str(dicts[i]['depart']) + '\n'
        j += 1
        k += 1
        #
        dicts[i]['new_passenger'] = float(tup[j])
        if len(product_list[k]) > 1:
            description_str += 'new passengers ' + str(dicts[i]['new_passenger']) + '\n'
        dicts[i]['description'] = description_str
        k = 0
        j = 0
        i += 1
    
    #returning...
    return dicts

#Computes average number of events from a given file that contains n simulation runs. If n is 1, just returns the number of events
#Accepts both strings and files as input, string are by default

def compute_statistics(n,file_name=None,string=None):
    #n is the number of experiments, file is the name of the file to open
    file_str = ''
    if file_name != None:
        file_handle = open(file_name, 'r')
        file_str = file_handle.read()
    else:
        file_str = string
    passenger_enters_re = re.compile(r"^A new passenger enters at stop (?P<origin_stop>\d+) with destination (?P<destination_stop>\d+) at time (?P<time>\d+\.\d+)\s*$",re.MULTILINE)
    passenger_boards_re = re.compile(r"^Passenger boards bus (?P<busid>\d+\.\d+) at stop (?P<origin_stop>\d+) with destination (?P<destination_stop>\d+) at time (?P<time>\d+\.\d+)\s*$", re.MULTILINE)
    passenger_disembarks_re = re.compile(r"^Passenger disembarks bus (?P<busid>\d+\.\d+) at stop (?P<destination_stop>\d+) at time (?P<time>\d+\.\d+)\s*$", re.MULTILINE)
    bus_departs_re = re.compile(r"^Bus (?P<busid>\d+\.\d+) leaves stop (?P<origin_stop>\d+) at time (?P<time>\d+\.\d+)\s*$", re.MULTILINE)
    bus_arrives_re = re.compile(r"^Bus (?P<busid>\d+\.\d+) arrives at stop (?P<destination_stop>\d+) at time (?P<time>\d+\.\d+)\s*$", re.MULTILINE)

    num_passengers = len(passenger_enters_re.findall(file_str))*1.0/n
    num_boards     = len(passenger_boards_re.findall(file_str))*1.0/n
    num_disembarks = len(passenger_disembarks_re.findall(file_str))*1.0/n
    num_departs     = len(bus_departs_re.findall(file_str))*1.0/n
    num_arrives    = len(bus_arrives_re.findall(file_str))*1.0/n
    return (num_passengers,num_boards,num_disembarks,num_departs,num_arrives)
                
#taken from stack overflow, for pretty-ness
#http://stackoverflow.com/questions/3160699/python-progress-bar/15860757#15860757
def update_progress(print_events,progress):
    if not print_events:
        return
    barLength = 20 # Modify this to change the length of the progress bar
    status = ""
    if isinstance(progress, int):
        progress = float(progress)
    if not isinstance(progress, float):
        progress = 0
        status = "error: progress var must be float\r\n"
    if progress < 0:
        progress = 0
        status = "Halt...\r\n"
    if progress >= 1:
        progress = 1
        sys.stdout.write("\r                                                                                   ")
        sys.stdout.flush()
        text = "\rPercent: [====================] 100% Done...\n"
        sys.stdout.write(text)
        sys.stdout.flush()
        return
    block = int(round(barLength*progress))
    text = "\rPercent: [{0}] {1}% {2}".format( "="*block + " "*(barLength-block), progress*100, status)
    sys.stdout.write(text)
    sys.stdout.flush()

class SimulationInputError(Exception):
    pass
class SimulationInputWarning(Exception):
    pass
