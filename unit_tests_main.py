#!/usr/bin/python
import time
import unit_tests
import unittest

suite_simulator_io = unittest.TestLoader().loadTestsFromTestCase(unit_tests.TestSimulatorIO)
suite_simulation_instance = unittest.TestLoader().loadTestsFromTestCase(unit_tests.TestSimulationInstance)
suite_simulation_execution = unittest.TestLoader().loadTestsFromTestCase(unit_tests.TestSimulationExecution)


alltests = unittest.TestSuite([suite_simulator_io,suite_simulation_instance,suite_simulation_execution])
unittest.TextTestRunner(verbosity=2).run(alltests)
