#!/usr/bin/python
import sys
from os.path import isdir
from optparse import OptionParser

import simulator_io
import simulation_collection

def main():
        parser = OptionParser()
        parser.add_option("-f",action="store",dest="input_file_name",help='''reads simulation from INPUT_FILE''',metavar="INPUT_FILE")
        parser.add_option("-o",action="store",dest="output_file_name",type="string",default=None,help='''outputs all events and statistics for all experimentations specified in INPUT_FILE into OUTPUT_FILE''',metavar="OUTPUT_FILE")
        parser.add_option("-n",action="store", type="int",dest="seed",default=None,help="sets the seed for the simulation to be the integer SEED. Uses python's random module which happens to use a Mersene Twister PNGR")
        parser.add_option("--disembark-before-board",action="store_true", dest="disembark_before_board",default=False,help="passengers may board a bus on a stop only if all passengers on the bus who want to disembark on the stop have disembarked from it; default case is that passengers may board a bus even when there are passengers who still wish to disembark")
        parser.add_option("--count-missed-passengers-multiple-times",action="store_true",dest="count_missed_passengers_multiple_times",default=False,help="if a passenger misses n consecutive buses on a stop due to full buses, then the passenger is counter as missed n times; default case is to count missed passenger once only.")
        parser.add_option("--post-simulation-validation",action="store_true",dest="post_simulation_validation",default=False,help="runs a post-simulation assertion test on the final state of the simulation. Specify if you wish to be even more certain that the simulation is not exhibiting any inconsistencies; off by default")
        #taken verbatim from http://docs.python.org/2/library/optparse.html
        parser.add_option("-v", action="store_true", dest="print_events", default=True, help="print to stdout; on by default")
        parser.add_option("-q", action="store_false", dest="print_events", help="suppress stdout printing; note that if you do not specify an output file and run the simulation with -q no record is made of the simulation run")
        (options, args) = parser.parse_args()
        if isdir('ENV/'):
                print 'Virtual Python Environment found. You are probably not working on DICE'
        else:
                print 'No virtual Python Environment found in local directory ENV.'
        if options.input_file_name == None:
                raise IOError("no input file given to simulation")
        collection = simulation_collection.SimulationCollection(options.input_file_name,seed = options.seed,disembark_before_board=options.disembark_before_board,count_missed_passengers_multiple_times=options.count_missed_passengers_multiple_times,print_events=options.print_events,output_file_name=options.output_file_name,post_simulation_validation=options.post_simulation_validation)
        output = collection.run_all_simulations()
        #we print if specified. By default, if there are experiments, the events list wont be printed
        #only stats will be printed
        if options.print_events:
                print output
        #if specified, everything gets dumped to a file
        collection.write_to_file()
        return 
                
if  __name__ =='__main__':
        main()

