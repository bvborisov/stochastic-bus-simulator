from collections import deque
try:
        import ipdb
except ImportError:
        import pdb

from simulator_io import SimulationInputError, SimulationInputWarning
class SimulationInstance:
	'''Static book-keeping class. See README for more information.'''
	def __init__(self,dict_parsed_file):
		self.routes = {} # the problem instance will have dicts of all the objects of interest
		self.roads = [] # roads are determined by two stops so its best to keep them in a list
		self.stops = {}
		self.buses = {}
		self.board = None
		self.disembarks = None
		self.departs = None
		self.new_passengers = None
		self.stop_time = None
		self.ignore_warnings = False
		self.optimise_parameters = False 
                self.description = ''
                self.error_str = ''
                self.warning_str = ''
		self.setup_simulation_from_dict(dict_parsed_file)
                self.validate_input()
        def as_dict(self):
                return {'route':self.routes,
                        'roads':self.roads,
                        'stops':self.stops,
                        'buses':[str(x) for x in self.buses.values()],
                        'board':self.board,
                        'disembarks':self.disembarks,
                        'departs':self.departs,
                        'new passengers':self.new_passengers,
                        'stop time':self.stop_time,
                        'ignore warnings':self.ignore_warnings,
                        'optimise parameters':self.optimise_parameters,
                        'description':self.description}
                
	# this expects as input a dictionary from the list of dictionaries
        # output by simulator_io.parse_input_file

	def setup_simulation_from_dict(self,dict_parsed_file):
		for _route in dict_parsed_file['routes']: #name clash with route module name
			# setting routes
			cur_route = Route()
			try:
				cur_route.set_number(int(_route[0]))
			except ValueError:
                                self.error_str +=  "Error: routes must have integer names\n"
				return
                        if cur_route.number in self.routes.keys():
                                self.error_str += "Error: Multiple definitions for route " + _route[0] + "\n"
                                return
                        # we deffer assigning the current route to the end of the _route loop since we need to 
			# add stops and buses to it !
			# setting stops
			cur_stops_order = [] # initialising outside the loop since it will not be in scope if it is inside !
			for _stop in _route[1]: # name clash with module stop, renaming to stop_
				cur_stop = Stop()
				try:
					cur_stop_num = int(_stop)
				except ValueError:
					self.error_str += "Error: stops must have integer names\n"
					return
				cur_stop.set_number(cur_stop_num)
				cur_route.append_stop(cur_stop_num) # most appropriate place to add stops -we just checked they are okay!
				cur_stops_order.append(cur_stop_num) # we add stops in the order that we see them i.e. in their respective order
				# IF the stop we just constructed is already present, i.e. it was added by a different iteration of the 
				# route loop, we add the new route number to its routes set
				# ELSE we just add the new stop to the stop dictionary (also setting its route)
				if cur_stop in self.stops.values():
					self.stops[cur_stop.number].add_route(cur_route.number)
				else:
					cur_stop.add_route(cur_route.number)
					self.stops[cur_stop.number] = cur_stop
			
			# setting buses
			try:
				num_buses = int(_route[2])
			except ValueError:
				self.error_str += "Error: Buses must be an integral number\n"
				return
                        if num_buses == 0:
                                self.warning_str += "Warning: No buses on route " + str(cur_route.number) + " specified\n"
			try:
				capacity = int(_route[3])
			except ValueError:
                                self.error_str += "Error: Buses must have integer capacities\n"
				return
                        if capacity == 0:
                                self.warning_str += "Warning: Buses on route " + str(cur_route.number) + " have capacity of 0\n"
			cur_bus = None
			for j in range(num_buses):
				cur_bus = Bus()
				cur_bus.set_route(cur_route)
				cur_bus.set_identifier(j)
				cur_bus.set_capacity(capacity)
				cur_route.append_bus(cur_bus.identifier)
				self.buses[cur_bus.identifier] = cur_bus
		# Now we set the route's stop_order and make it a tuple since we desire immutability	
			cur_route.set_stops_order(tuple(cur_stops_order)) 
		# finally add the curent route to the dict of routes
		# having added its stops and buses
			self.routes[cur_route.number] = cur_route
		# roads handling
		for _road in dict_parsed_file['roads']:
                        try:
                                first_stop = self.stops[int(_road[0])]
                        except KeyError:
                                self.error_str += "Error: Road references stop " + str(int(_road[0])) + " which does not appear on any route\n"
                        try:
                                second_stop = self.stops[int(_road[1])]
                        except KeyError:
                                self.error_str += "Error: Road references stop " + str(int(_road[1])) + " which does not appear on any route\n"
			cur_road = Road(first_stop, second_stop, float(_road[2]))
			self.roads.append(cur_road)
		# board, disembarks, etc., handling
		self.board = dict_parsed_file['board']
		self.disembarks = dict_parsed_file['disembark']
		self.departs = dict_parsed_file['depart']
		self.new_passengers = dict_parsed_file['new_passenger']
		self.stop_time = dict_parsed_file['stop_time']
		self.ignore_warnings = dict_parsed_file['ignore warnings']
		self.optimise_parameters = dict_parsed_file['optimise parameters']
                self.description = dict_parsed_file['description']

	def __str__(self):
		return str(self.as_dict())

        def validate_input(self):
                '''This method runs some validation on the simulation instance that is not easy to run on the input file i.e. it is more advnaced than the first layer of validation in parse_input_file since it relies on the structure of the simulation instance. It is also more advanced than the second layer of validation when we construct the simulation instance itself since that just checks for negative identifier which should not happen at all.'''
                # if the set_up code returned prematurely we catch it here
                if self.error_str != '':
                        raise SimulationInputError(self.error_str)
                error_str = ''
                warning_str = ''
                # checks if there is a road between a stop and
                # itself or if there is more than 1 road between 2 stops
                # or if road between stops is not on any route
                for stop1 in self.stops.values():
                        for stop2 in self.stops.values():
                                # len(roads) is num of roads between 2 stops
                                roads = [road for road in self.roads if road.first_stop == stop1 and road.second_stop == stop2]
                                if stop1 != stop2:
                                        if len(roads) > 1:
                                                error_str += 'Error: Multiple roads defined between stops ' + str(stop1.number) + ' and ' + str(stop2.number) + '\n'
                                        if len(roads) == 1 and (stop1.routes & stop2.routes == set([])):
                                                warning_str += 'Warning: Road between stops ' + str(stop1.number) + ' and ' + str(stop2.number) + ' is not on any defined route\n'
                                if stop1 == stop2 and len(roads) > 0:   # if stops are the same
                                        error_str += 'Error: Road defined between stop ' + str(stop1.number) + ' and itself \n'
                # this checks if there are consecutive stops on a route for which there is no road
                for route in self.routes.values():
                        i = 0
                        for i in range(len(route.stops_order)):
                                roads = [road for road in self.roads if road.first_stop.number == route.stops_order[i] and road.second_stop.number == route.stops_order[(i+1) % len(route.stops_order)]]
                                if len(roads) == 0:
                                        error_str += 'Error: No road defined between consecutive stops ' + str(route.stops_order[i]) + ' and ' + str(route.stops_order [(i+1) % len(route.stops_order) ]) + ' on route ' + str(route.number) + '\n'
                                i += 1
                
                for road in self.roads:
                        if road.rate == 0.0 or road.rate == 0:
                                error_str += 'Error: ' + str(road)

                count = 0
                if self.board == 0.0 or self.board == 0:
                        warning_str += 'Warning: Board rate is 0\n'
                        count += 1
                if self.disembarks == 0.0 or self.disembarks == 0:
                        warning_str += 'Warning: Disembark rate is 0\n'
                        count += 1
                if self.departs == 0.0 or self.departs == 0:
                        warning_str += 'Warning: Departs rate is 0\n'
                        count += 1
                if self.new_passengers == 0.0 or self.new_passengers == 0:
                        warning_str += 'Warning: New passengers rate is 0\n'
                        count += 1
                if self.stop_time == 0.0 or self.stop_time == 0:
                        warning_str += 'Warning: Stop time is is 0\n'
                        count += 1
                if count >= 2:
                        error_str += "Error: " + str(count) + " control rates are not set\n"
                if error_str != '':
                        raise SimulationInputError(error_str)
                if self.warning_str + warning_str != '' and self.ignore_warnings == False:
                        raise SimulationInputWarning(self.warning_str + warning_str)
                return
class Bus:
	def __init__(self):
		self._route = None
		self.capacity = 0 # all buses that follow the same route have the same capacity - not enforced right now
		self.identifier = ''
		self.state = None # bus either at stop or on road
		self._passengers = set([]) # bus has a set of passengers 

	def set_route(self,_route):
		self._route = _route

	def set_capacity(self,capacity):
		self.capacity = capacity

	def set_identifier(self,bus_number):
		self.identifier = str(self._route.number) + '.' + str(bus_number)

	def __str__(self):
		string =  "Identifier: " + self.identifier + "\n"
		string += "Route number: " + str(self._route.number) + "\n"
		string += "Capacity: " + str(self.capacity) + "\n"
		string += "State: " + str(self.state) + "\n"
		string += "Passengers: " + str(self._passengers)
		return string

	def set_state(self,state_tuple): #expected ot be tuple
		self.state = state_tuple
		
	def add_passenger(self,passenger):
		self._passengers.add(passenger)

        def passengers_waiting_to_disembark_cur_stop(self):
                pass_wait_list = []
                for x in self._passengers:
                        # if there are people who want to get off this stop
                        if x.destination_stop == self.state[1]:
                                pass_wait_list.append(x)
                return pass_wait_list
        # optimised short-circuit version
        def are_passengers_waiting_to_disembark_cur_stop(self):
                for x in self._passengers:
                # if there are people who want to get off this stop
                        if x.destination_stop == self.state[1]:
                                return True
                return False

        def has_capacity(self):
                return len(self._passengers) < self.capacity
        
        def __eq__(self, other):
                if isinstance(other, Bus):
                        return self.identifier == other.identifier
                return NotImplemented
        
        def __neq__(self, other):
                return not self.__eq__(other)
        
#Roads hold references to stops instead of just stop numbers
class Road:
	def __init__(self,first_stop,second_stop,rate):
                self.first_stop = first_stop
		self.second_stop = second_stop
		self.rate = rate
        def __str__(self):
                return 'Road between stops ' + str(self.first_stop.number) + ' and ' + str(self.second_stop.number) + ' with rate ' + str(self.rate) + '\n'

class Route:
	def __init__(self):
		self.stops = [] # our stops will be represented by a list of stops
		self.buses = [] # each route has a number of buses associated with it
		self.number = None # a route has a unique number
		self.stops_order = None # the order of the stops as in the input file, should be immutable, so tuple is a good choice

	def set_number(self,number):
		self.number = number

	def set_stops(self,stop_list):
		self.stops = stop_list

	def set_buses(self,bus_list):
		self.buses = bus_list

	def append_stop(self,stop_num):
		self.stops.append(stop_num)

	def append_bus(self,bus_ident):
		self.buses.append(bus_ident)
		
	def set_stops_order(self,order_tuple):
		self.stops_order = order_tuple

class Stop:
	def __init__(self):
		self.bus_queue = deque([]) # We adopt the convention that we append to the right and pop from the left!
		self.passenger_set = set([]) # holding actual references to passengers
		self.routes = set([]) # list of all routes that have this stop
		self.number = None

	def set_number(self,number):
		self.number = number

	def add_route(self,route_number):
		self.routes.add(route_number)

	def __eq__(self, other):
		if isinstance(other, Stop):
			return self.number == other.number
		return NotImplemented
		
	def __str__(self):
		string = "Stop number: " + str(self.number) + "\n"
		string += "Routes: " + str(self.routes) + "\n"
		string += "Bus queue: " + str(self.bus_queue) + "\n"
		string += "Passenger set: " + str(self.passenger_set)
		return string

        def __neq__(self, other):
                return not self.__eq__(other)
        
        def get_passengers_waiting_for_bus(self,bus):
                pass_wait_list = []
                # iterate over all passengers and return list of all passengers who want to board bus
                for pas in self.passenger_set:
                        # if cur passenger is going in the direction of the route of the given bus
                        if pas.destination_route == bus._route.number: # BUSES HOLD REFERENCES TO ROUTES, NOT JUST ROUTE NUMS
                                pass_wait_list.append(pas)
                return pass_wait_list
        # optimised short-circuit version
        def are_passengers_waiting_for_bus(self,bus):
                # iterate over all passengers and return list of all passengers who want to board bus
                for pas in self.passenger_set:
                        # if cur passenger is going in the direction of the route of the given bus
                        if pas.destination_route == bus._route.number: # BUSES HOLD REFERENCES TO ROUTES, NOT JUST ROUTE NUMS
                                return True
                return False

class Passenger:
	"""Passenger class 
	we assume the constructor creates a passenger that adheres to all the constraints laid our in the practical handout"""
	global_identifier_count = long(0) # we can have as many passengers as we want
	def __init__(self):
		self.identifier = Passenger.global_identifier_count
		Passenger.global_identifier_count += 1
		self.origin_stop = None
		self.destination_stop = None
		self.state = None
		self.destination_route = None # it would be easier to manage things with this

	def set_origin_stop(self,_stop):
		self.origin_stop = _stop

	def set_destination_stop(self,_stop):
		self.destination_stop = _stop

	def set_state(self,state):
		self.state = state

	def set_destination_route(self,route_num):
		self.destination_route = route_num

	def __eq__(self,other):
		if isinstance(other, Passenger):
                        return self.identifier == other.identifier

	def __ne__(self,other):
		return not self.__eq__(other)
        
	def __str__(self):
		string = "Passenger identifier: " + str(self.identifier) + "\n"
		string += "Origin stop: " + str(self.origin_stop) + "\n"
		string += "Destination stop: " +  str(self.destination_stop) + "\n"
		string += "State: " +  str(self.state) + "\n"
		string += "Destination route: " + str(self.destination_route) + "\n"
		return string
        # needed to be able to put Passengers in sets
	def __hash__(self):
		return hash(str(self.identifier))
		
	# needed mostly for testing if we use the same execution 
	@staticmethod
	def reset_global_counter():
		Passenger.global_identifier_count = long(0)
		

