Bus Simulator
==================

Computer Science Large Practical 2013/2014
------------------------------------------

### Author: Bilyan Borisov

### Part 1 - general details

**Introduction**

This is a command line simulator for bus networks written in Python, version 2.6. It can parse files as in the CSLP handout specification and it produces as output events of the bus simulation as well as statistics and, if experimentations are present, it does simple parameter optimisation to try and find the best configuration.

**The input format**

The simulator accepts input as specified in the CSLP handout which for convenience I have copied below:

We need a description of the bus network and the number of buses in the network. To keep
things simple we will use a line-based format. Each object is described in one line of input. The
lines of input describing an object are either a route line or a road line.
The routes are described as a set of numbers. The first number is the route name and
following are the lists of stops. Following this we describe the number of buses on that route
and the capacity of each bus on that route. Here is an example:

route 1 stops 1 2 3 buses 4 capacity 50

Describes route number 1 which stops at stops 1, 2 and 3. There are 4 buses in operation on
that route and each of those buses has a capacity of 50. The general form of a route line is there-
fore: 
route <integer> stops <integer>+ buses <integer> capacity <integer> 

Though there should be at least two integers in any route.
For the purposes of output, which is described below, the sequence of stops are identifiers
for stop numbers. Each bus has a unique identifier of the form <integer>.<integer> which is
the route and the number of the bus on that route. Note that more than one route may visit
the same stop, but a particular bus can only be on one route. The unique bus numbers for the
example above would be: 1.0, 1.1, 1.2, and 1.3.

We then need to describe how long on average it takes for a bus to travel between two
particular stops. For example:

road 1 2 0.3

Describes the road between stops 1 and 2. The rate at which a bus traverses the road is 0.3
meaning that it will take 1/0.3 time units on average. Note that there should be exactly one road
description line for all pairs of stops that occur adjacently in at least one route. Additionally
recall that there is an implicit road between the last and first stops since all routes are circular.
The general form for a road line is therefore: road <integer> <integer> <float>

In addition to the lines specifying the buses and routes, there are some further control lines
which describe variables within the simulation.

board <float>

disembarks <float>

departs <float>

new passengers <float>

stop time <float>

ignore warnings

optimise parameters

Each of these lines sets a variable used within the simulation. These are:

• board is always present and specifies the rate at which passengers board a bus, only one
can do so at a time.

• disembarks is always present and specifies the rate at which passengers disembark a bus,
only one can do so at a time

• departs is always present and specifies the rate at which a bus departs from a stop

• new passengers the general rate at which new passengers arrive into the network

• stop time is always present and specifies the stop time of the simulation

• ignore warnings is either present or absent and will be discussed in the “Validation”
sub-section below.

• optimise parameters is either present or absent and will be discussed in the “Parameter
Optimisation” sub-section below.

All of the numbers here are floating point numbers. The ignore warnings and optimise
parameters commands are either present or absent whereas each of the other commands should
appear exactly once.

Finally, input lines can occur in any order and comments are begun with the # character
and ended with a new line. Blank lines are also ignored.

Comments are also accepted starting with # and they can be both inline or on new lines. Control lines also do not have to be on the begining of the line (but putting spaces in between words on the line is *NOT* generally advised)

The keyword `experiments` specifies a list of values that the simulation should all try out.

**How to use it**

This is a copy of the output as given by simulator.py -h

For example usage, see Example usage below

 Usage: simulator.py [options]
` 
 Options:

   -h, --help            show this help message and exit

   -f INPUT_FILE         reads simulation from INPUT\_FILE

   -o OUTPUT_FILE        outputs all events and statistics for all
                         experimentations specified in INPUT\_FILE into
                         OUTPUT\_FILE

   -n SEED               sets the seed for the simulation to be the integer
                         SEED. Uses python's random module which happens to use
                         a Mersene Twister PNGR

   --disembark-before-board

                         passengers may board a bus on a stop only if all
                         passengers on the bus who want to disembark on the
                         stop have disembarked from it; default case is that
                         passengers may board a bus even when there are
                         passengers who still wish to disembark

   --count-missed-passengers-multiple-times

                         if a passenger misses n consecutive buses on a stop
                         due to full buses, then the passenger is counter as
                         missed n times; default case is to count missed
                         passenger once only.

   --post-simulation-validation

                         runs a post-simulation assertion test on the final
                         state of the simulation. Specify if you wish to be
                         even more certain that the simulation is not
                         exhibiting any inconsistencies; off by default

   -v                    print to stdout; on by default

   -q                    suppress stdout printing; note that if you do not
                         specify an output file and run the simulation with -q
                         no record is made of the simulation run`
 

`-f INPUT_FILE`

The only obligatory command line argument is the input file and it is supplied with -f followed by its name. If not given, the program throws an IOError Exception

`-o OUTPUT_FILE`

Optional argument. Writes the full output produced by the simulation : for every experimentation combination configuration(if any) it generates the configuration, followed by the list of events output by the simulation, and followed by the statistics for the simulation and a new line. If the parameter optimisation control line is present in the INPUT\_FILE, then finally outputs the least cost configuration and its cost. All of this is written to the file OUTPUT_FILE. **NOTE** writing to file is different from printing to stdout, see -v and -q below for details on printing.

`-n SEED`

Optional argument. If present, runs the simulation by setting the seed to be the integer SEED. This is for reproducing results. Note that python uses the Mersene Twister Pseudo Random Number Generator http://docs.python.org/2.6/library/random.html . If not present, a random seed is used.

`--disembark-before-board`

Optional experimental feature. If flag is present, it will force the simulation to make passengers waiting for a bus on a stop to board it only after all passengers who have disembarked on that stop have done so. This might be useful in some bus networks where passengers board at the front door, for example Edinburgh. The default is to let passengers board and disembark at the same time, like in Berlin for example.

`--count-missed-passengers-multiple-times`

Optional experimental feature. If flag is present, it will count a passenger, who has missed n consecutive buses due to the them being full, as missed n times. The default setting is to count him only once regardless of the number of buses he misses.
	
`--post-simulation-validation`

Optional post-simulation test. If flag is present, this will run a series of assertion tests on the final state of the simulation which will verify that there are no inconsistencies. These tests are not exhaustive but they do cover a variety of failures. Naturally, this slows down the execution time. **NOTE** - this does not run the unit tests - for the see below. This is a sort of a 'dynamic-test' function that is there to assure the user 'a little bit more' that this stochastic simulator is outputting consistent events.

`-v`

Optional argument, set by default. This controls whether the simulation will print its output to stdout. **NOTE** regardless of this being set, the default behaviour for when INPUT\_FILE contains experiments is to only output the experiments configuration and the summary statistics i.e. even with -v, if the file contains experiments the simulation events won't be printed! This was done in light of the fact that if experiments are present, usually one is interested in the summary statistics and the experiment configuration. However, if one still wants to look at the generated events, then specifying an OUTPUT\_FILE with -o will dump everything that the simulator produces as output into that file

`-q`

Optional argument, unset by default and it is the complement of -v. If set, it will supress printing to stdout. **NOTE** if a simulation is run from a file that contains experiments and no OUTPUT\_FILE is specified with -o and -q is also given, then all output by the simulation is lost! Therefor only use this flag if you want to suppress output since you are saving into a file anyway.

**Example usage**

In the folder test_files there are a number of files that are used for unit tests and they should not be changed. However, a number of files are present only for running the simulation on them and they can be used as follows:

`python simulator.py -f test_files/simple12.text`

This will just run the simulation with file simple12.text and will print to std out. Use

`python simulator.py -f test_files/simple12.text -o 12.out` 

to save the output it produced to file 12.out and

`python simulator.py -f test_files/simple12.text -o 12_2.out -q` to not print to stdout while saving to 12_2.out

`python simulator.py -f test_files/simple12.text -n 12345678` will run the simulation with seed 12345678 and thus, if you give it back the same seed it will produce the same set of events.

`python simulator.py -f test_files/simple10.text -o 10.out` will run the simulation from file simple10.text which has experiments and optimise parameters and thus only the statistics (not the events) for every configuration will be printed but ALL the output will be save to 10.out

`python simulator.py -f test_files/simple8.text -o 8.out --disembark-before-board -n 1234 --post-simulation-validation -q` will run the simulation from file simple8.text, it will force passengers to disembark before others can board, it will use the seed 1234, it will save the output to 8.out, it will run post simulation tests and it will not print to stdout (and it will do parameter optimisation)

All the files in the above examples are commited into the git repository and can be inspected - they are in the sample_output dir

### Part 2 - implementation and developer details

**Tests**

The tests for the simulator can be run either with the 

`./tests` script 

or by running `python unit_tests_main.py` . The tests take about 17 seconds to complete on a modern, regular purpose PC.

Tests are not exhaustive of the code but they do cover most of it. The Python coverage report can be seen in <a href='htmlcov/index.html'> here</a> or if this doesnt open or display go to htmlcov/index.html

**Development details**

The simulator is written in Python 2.6 and was mostly developed on a x64 Linux Mint machine (my own laptop) with the same version of Python as on DICE. I used a python virtual environment, which I set up in the directory ENV (not present in the repository). I used emacs as an editor and IPython as an IDE.

**Code optimisation**

I used cProfile, a python profiler, to profile my code. I found out that a couple of functions, namely are\_passengers\_waiting\_to\_disembark\_cur\_stop(), are\_passengers\_waiting\_for\_bus(), and their get versions, as well as the discrete\_dist\_samples method in simulation_execution are slow and I rewrote them. To see the profiling, you must open ipython(or plain python) load module pstats and type

`p = pstats.Stats('simstats')`

`p.strip_dirts().sort_stats('cumulative').print_stats(40)`

to see the top 40 methods sorted in cumulative time

**Project Architecture**

I did not change anything in a fundametal way in the architecture of the project from the submission for part1. I did move all the small classes into the file for simulation\_instace (as suggested). So, just a broad, high-level view of my code structure:

`simulation_instance.py`

Contains 

SimulationInstance() class which is a static, or state, representation of a simulation. It is constructed from a dictionary as output by simulator\_io.parse\_input\_file(). It is intended to all object references (but this is not always the case). It also performs more fine-grained and sophisticated validation, see below.

Bus, Road, Route, Stop, Passenger classes - they represent the entities of our simulation. When they would reference each-other I tried to use identifier instead of actual objects ( say Passenger() instances have a origin\_stop attribute but that is not an actual reference to a stop but its integer identifier)

`simulation_execution.py`

Contains SimulationExecution() - a class that is the dynamic representation of a simulation, it contaisn the most import function, run\_simulation() which runs the simulation. A SimulationExecution has its own SimulationInstance. The file also contains the 5 event classes: PassengerEntersSimulation(), PassengerBoards(), BusDepartsStop(), BusArrivesAtStop(), PassengerDisembarksBus() and I removed their common superclass, Event, since part 1. The file also contains 2 general purpose functions that are used inside SimulationExecution but I decided that having them as member functions was an overkill : uzip and normalise.

`simulator_io.py`

Contains the main IO function that does the initial parsing of the input files : parse\_input\_file(). The function produces a list of dictionaries, one for each combination of experimentation values and it also does the first, coarse level of validation. For more on validation, see below. File also contains Exception mini classes - SimulationInputError and SimulationInputWarning, update\_progress() - a progress bar, dislayed only when there are no experiments, and compute_statistics() : a method that computes the average number of events given a file with multiple levels runs of a simulation. This is an experiment function and it was nice having it as a debug tool but I decided not to include it in the simulator itself.

`simulation_collection.py`

Contains SimulationCollection() - a class that holds multiple SimulationExecutions, each one for a given experimentation combination. It takes a number of parameters in its constructors that are passed to it in simulator.py from the command line. The method run\_all\_simulations() runs all simulations

`simulator.py`

Contains the code that parses the command line arguments and produces a running simulation.

`unit_tests.py and unit_tests_main.py`

My unit tests, mostly self explanatory

**Validation**

Validation was done on three levels - in parse\_input\_files, where it was course i.e. if we, say, didn't specify any routes, in SimulationInstance's setup\_simulation\_from\_dict(), where it was more fine i.e. if we, say, gave 2 routes the same identifier, and in validate\_input() (again in SimulationInstace) where it was more sophisticated i.e. we look for things like if there are 2 consecutive stops for which there is no road. 

We have 2 types of validation misfits: errors and warnings. My idea was to permit the code catch as much errors and warnings as possible in the 3rd validation level i.e. in validate\_input() since these were hard to spot errors that would create a SimulationInstance that is potentially runable, and to stop execution when an error(or warning) was spotted in the 1st and 2nd validation levels(in parse\_input\_file() and setup\_simulation\_from_dict()) since these were gross errors that could have lead to other, unhandled exceptions in the parsing code. 

If the control line ignore warnings was present, the warnings are ignored, but otherwise the execution will halt even if there is a warning.

**Experimentation**

Experiments were implemented as specified in the CSLP handout. From the simulation's point of view, there are no experiments and that's why I have a SimulationCollection() class that has a list of SimulationExecutions.

**Analysis**

Analysis is output as specified. The extra feature I added (count passengers multiple times) is as described in **How to use it** .

**Parameter Optimisation**

Parameter optimisation is quite crude - the cost function proposed in the CSLP handout it used i.e. (board rate + disembarks rate)*overall number of missed passengers. 

**Git repository**

I made use of branches again, I also used rebasing,stashing, and I tried to keep my repository clean,out of junk, and well documented

**TODO**

I'm using a bash grep script for this, as suggested

**README**

This readme was generated with a mardown generator from here http://daringfireball.net/projects/markdown/ , and it is also commited in the repository. To use try

`perl Markdown.pl README.md > some_file.html`